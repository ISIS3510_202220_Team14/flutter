import 'package:firebase_analytics/firebase_analytics.dart';

FirebaseAnalytics analytics = FirebaseAnalytics.instance;

Future<void> sendFeatureEvent(String text) async {
  analytics.logEvent(name: 'event_feature', parameters: <String, dynamic>{
    'feature': text,
  });
}

Future<void> sendReadingPlanClickEvent(String text) async {
  analytics
      .logEvent(name: 'event_readingPlanClicked', parameters: <String, dynamic>{
    'readingPlan': text,
  });
}

Future<void> sendDiscussionEvent(String text) async {
  analytics
      .logEvent(name: 'event_discussionClicked', parameters: <String, dynamic>{
    'readingPlan': text,
  });
}
