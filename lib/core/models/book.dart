import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'book.g.dart';

@JsonSerializable()
class Book {
  final String id;
  final String name;
  final String isbn;
  final String imageUrl;
  final int numPages;
  final String author;
  final String genre;

  const Book({
    required this.id,
    required this.name,
    required this.isbn,
    required this.imageUrl,
    required this.numPages,
    required this.author,
    required this.genre,
  });

  factory Book.fromSnapshot(DocumentSnapshot<Map<String, dynamic>> snapshot) =>
      _$BookFromJson({
        ...?snapshot.data(),
        'id': snapshot.id,
      });

  Map<String, dynamic> toJson() => _$BookToJson(this);
}

@JsonSerializable()
class CreateBook {
  final String name;
  final String isbn;
  final String imageUrl;
  final int numPages;
  final String author;
  final String genre;

  const CreateBook(
      {required this.name,
      required this.isbn,
      required this.imageUrl,
      required this.numPages,
      required this.author,
      required this.genre});

  Map<String, dynamic> toJson() => _$CreateBookToJson(this);
}

const nullBook = Book(
    id: '?',
    name: 'BookUnavailable',
    isbn: '?',
    imageUrl: '?',
    numPages: 0,
    author: '?',
    genre: '?');
