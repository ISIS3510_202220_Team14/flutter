// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Book _$BookFromJson(Map<String, dynamic> json) => Book(
      id: json['id'] as String,
      name: json['name'] as String,
      isbn: json['isbn'] as String,
      imageUrl: json['imageUrl'] as String,
      numPages: json['numPages'] as int,
      author: json['author'] as String,
      genre: json['genre'] as String,
    );

Map<String, dynamic> _$BookToJson(Book instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'isbn': instance.isbn,
      'imageUrl': instance.imageUrl,
      'numPages': instance.numPages,
      'author': instance.author,
      'genre': instance.genre,
    };

CreateBook _$CreateBookFromJson(Map<String, dynamic> json) => CreateBook(
      name: json['name'] as String,
      isbn: json['isbn'] as String,
      imageUrl: json['imageUrl'] as String,
      numPages: json['numPages'] as int,
      author: json['author'] as String,
      genre: json['genre'] as String,
    );

Map<String, dynamic> _$CreateBookToJson(CreateBook instance) =>
    <String, dynamic>{
      'name': instance.name,
      'isbn': instance.isbn,
      'imageUrl': instance.imageUrl,
      'numPages': instance.numPages,
      'author': instance.author,
      'genre': instance.genre,
    };
