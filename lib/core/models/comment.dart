import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'comment.g.dart';

@JsonSerializable()
class Comment {
  final String id;
  final String text;
  final String userId;

  const Comment({
    required this.id,
    required this.text,
    required this.userId,
  });

  factory Comment.fromSnapshot(
          DocumentSnapshot<Map<String, dynamic>> snapshot) =>
      _$CommentFromJson({
        ...?snapshot.data(),
        'id': snapshot.id,
      });

  Map<String, dynamic> toJson() => _$CommentToJson(this);
}

const nullComment = Comment(id: '?', text: 'CommentUnavailable', userId: '?');
