import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'discussion.g.dart';

@JsonSerializable()
class Discussion {
  final String id;
  final String topic;
  final List<String> commentsIds;
  final int views;

  const Discussion({
    required this.id,
    required this.topic,
    required this.commentsIds,
    required this.views,
  });

  factory Discussion.fromSnapshot(
          DocumentSnapshot<Map<String, dynamic>> snapshot) =>
      _$DiscussionFromJson({
        ...?snapshot.data(),
        'id': snapshot.id,
      });

  Map<String, dynamic> toJson() => _$DiscussionToJson(this);
}

const nullDiscussion = Discussion(
    id: '?', topic: 'DiscussionUnavailable', commentsIds: [], views: 0);
