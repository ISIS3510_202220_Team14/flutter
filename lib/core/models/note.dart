import 'package:bookfriends/core/models/timestamp_converter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'note.g.dart';

@JsonSerializable()
class Note {
  final String id;
  final String userId;
  final String title;
  final String description;
  final String bookId;
  @TimestampConverter()
  final DateTime timestamp;

  const Note({
    required this.id,
    required this.userId,
    required this.title,
    required this.description,
    required this.bookId,
    required this.timestamp,
  });

  factory Note.fromSnapshot(DocumentSnapshot<Map<String, dynamic>> snapshot) =>
      _$NoteFromJson({
        ...?snapshot.data(),
        'id': snapshot.id,
      });

  Map<String, dynamic> toJson() => _$NoteToJson(this);
}

@JsonSerializable()
class CreateNote {
  final String userId;
  final String title;
  final String description;
  final String bookId;
  @TimestampConverter()
  final DateTime timestamp;

  const CreateNote({
    required this.userId,
    required this.title,
    required this.description,
    required this.bookId,
    required this.timestamp,
  });

  Map<String, dynamic> toJson() => _$CreateNoteToJson(this);
}

final nullNote = Note(
  id: '?',
  userId: '?',
  title: 'NoteUnavailable',
  description: '',
  bookId: '',
  timestamp: DateTime.now(),
);
