import 'package:geolocator/geolocator.dart';

class PlaceList {
  List<Place> list;

  PlaceList({
    required this.list,
  });

  factory PlaceList.fromJson(
      List<dynamic> parsedJson, double latInit, double longInit) {
    var lists = parsedJson;

    List<Place> placeLists =
        lists.map((f) => Place.fromJson(f, latInit, longInit)).toList();

    return PlaceList(list: placeLists);
  }
}

class Place {
  String name, rating, vicinity;
  double distance, lat, long;

  Place({
    required this.name,
    required this.vicinity,
    required this.rating,
    required this.distance,
    required this.lat,
    required this.long,
  });

  factory Place.fromJson(
      Map<String, dynamic> parsedJson, double latInit, double longInit) {
    String rate = '';
    if (parsedJson['rating'] != null) {
      rate = parsedJson['rating'].toString();
    }

    double lat = parsedJson['geometry']['location']['lat'];
    double long = parsedJson['geometry']['location']['lng'];

    double val =
        Geolocator.distanceBetween(latInit, longInit, lat, long) / 1000;

    return Place(
      name: parsedJson['name'],
      rating: rate,
      distance: double.parse((val).toStringAsFixed(2)),
      vicinity: parsedJson['vicinity'],
      lat: parsedJson['geometry']['location']['lat'],
      long: parsedJson['geometry']['location']['lng'],
    );
  }
}
