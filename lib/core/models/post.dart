import 'package:bookfriends/core/models/timestamp_converter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post.g.dart';

@JsonSerializable()
class Post {
  final String id;
  final String userId;
  final String content;
  final int likes;
  final int numComments;
  final int numShares;
  @TimestampConverter()
  final DateTime publicationDate;
  final String readingPlanId;

  const Post({
    required this.id,
    required this.userId,
    required this.content,
    required this.likes,
    required this.numComments,
    required this.numShares,
    required this.publicationDate,
    required this.readingPlanId,
  });

  factory Post.fromSnapshot(DocumentSnapshot<Map<String, dynamic>> snapshot) => _$PostFromJson({
        ...?snapshot.data(),
        'id': snapshot.id,
      });

  Map<String, dynamic> toJson() => _$PostToJson(this);
}

@JsonSerializable()
class CreatePost {
  final String userId;
  final String content;
  final int likes;
  final int numComments;
  final int numShares;
  @TimestampConverter()
  final DateTime publicationDate;
  final String readingPlanId;

  const CreatePost({
    required this.userId,
    required this.content,
    this.likes = 0,
    this.numComments = 0,
    this.numShares = 0,
    required this.publicationDate,
    required this.readingPlanId,
  });

  Map<String, dynamic> toJson() => _$CreatePostToJson(this);
}

final nullPost = Post(
  id: '?',
  userId: '?',
  content: 'PostUnavailable',
  likes: 0,
  numComments: 0,
  numShares: 0,
  publicationDate: DateTime.now(),
  readingPlanId: '?',
);
