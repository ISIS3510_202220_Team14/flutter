// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Post _$PostFromJson(Map<String, dynamic> json) => Post(
      id: json['id'] as String,
      userId: json['userId'] as String,
      content: json['content'] as String,
      likes: json['likes'] as int,
      numComments: json['numComments'] as int,
      numShares: json['numShares'] as int,
      publicationDate: const TimestampConverter()
          .fromJson(json['publicationDate'] as Timestamp),
      readingPlanId: json['readingPlanId'] as String,
    );

Map<String, dynamic> _$PostToJson(Post instance) => <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'content': instance.content,
      'likes': instance.likes,
      'numComments': instance.numComments,
      'numShares': instance.numShares,
      'publicationDate':
          const TimestampConverter().toJson(instance.publicationDate),
      'readingPlanId': instance.readingPlanId,
    };

CreatePost _$CreatePostFromJson(Map<String, dynamic> json) => CreatePost(
      userId: json['userId'] as String,
      content: json['content'] as String,
      likes: json['likes'] as int? ?? 0,
      numComments: json['numComments'] as int? ?? 0,
      numShares: json['numShares'] as int? ?? 0,
      publicationDate: const TimestampConverter()
          .fromJson(json['publicationDate'] as Timestamp),
      readingPlanId: json['readingPlanId'] as String,
    );

Map<String, dynamic> _$CreatePostToJson(CreatePost instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'content': instance.content,
      'likes': instance.likes,
      'numComments': instance.numComments,
      'numShares': instance.numShares,
      'publicationDate':
          const TimestampConverter().toJson(instance.publicationDate),
      'readingPlanId': instance.readingPlanId,
    };
