import 'package:bookfriends/core/models/timestamp_converter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'reading_plan.g.dart';

@JsonSerializable()
class ReadingPlan {
  final String id;
  final String title;
  final List<String> usersIds;
  final List<String> discussionsIds;
  final String bookId;
  @TimestampConverter()
  final DateTime startDate;
  @TimestampConverter()
  final DateTime? endDate;
  final int views;

  const ReadingPlan({
    required this.id,
    required this.title,
    required this.usersIds,
    required this.discussionsIds,
    required this.bookId,
    this.endDate,
    required this.startDate,
    required this.views,
  });

  factory ReadingPlan.fromSnapshot(DocumentSnapshot<Map<String, dynamic>> snapshot) => _$ReadingPlanFromJson({
        ...?snapshot.data(),
        'id': snapshot.id,
      });

  Map<String, dynamic> toJson() => _$ReadingPlanToJson(this);
}

@JsonSerializable()
class CreateReadingPlan {
  final String title;
  final List<String> usersIds;
  final List<String> discussionsIds;
  final String bookId;
  @TimestampConverter()
  final DateTime startDate;
  @TimestampConverter()
  final DateTime? endDate;
  final int views;

  const CreateReadingPlan({
    required this.title,
    required this.usersIds,
    this.discussionsIds = const [],
    required this.bookId,
    required this.startDate,
    required this.endDate,
    this.views = 0,
  });

  Map<String, dynamic> toJson() => _$CreateReadingPlanToJson(this);
}

final nullReadingPlan = ReadingPlan(
    id: '?', title: 'noTitle', usersIds: [], discussionsIds: [], bookId: '?', startDate: DateTime.now(), views: 0);
