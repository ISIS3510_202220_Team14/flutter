// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reading_plan.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadingPlan _$ReadingPlanFromJson(Map<String, dynamic> json) => ReadingPlan(
      id: json['id'] as String,
      title: json['title'] as String,
      usersIds:
          (json['usersIds'] as List<dynamic>).map((e) => e as String).toList(),
      discussionsIds: (json['discussionsIds'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      bookId: json['bookId'] as String,
      endDate: _$JsonConverterFromJson<Timestamp, DateTime>(
          json['endDate'], const TimestampConverter().fromJson),
      startDate:
          const TimestampConverter().fromJson(json['startDate'] as Timestamp),
      views: json['views'] as int,
    );

Map<String, dynamic> _$ReadingPlanToJson(ReadingPlan instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'usersIds': instance.usersIds,
      'discussionsIds': instance.discussionsIds,
      'bookId': instance.bookId,
      'startDate': const TimestampConverter().toJson(instance.startDate),
      'endDate': _$JsonConverterToJson<Timestamp, DateTime>(
          instance.endDate, const TimestampConverter().toJson),
      'views': instance.views,
    };

Value? _$JsonConverterFromJson<Json, Value>(
  Object? json,
  Value? Function(Json json) fromJson,
) =>
    json == null ? null : fromJson(json as Json);

Json? _$JsonConverterToJson<Json, Value>(
  Value? value,
  Json? Function(Value value) toJson,
) =>
    value == null ? null : toJson(value);

CreateReadingPlan _$CreateReadingPlanFromJson(Map<String, dynamic> json) =>
    CreateReadingPlan(
      title: json['title'] as String,
      usersIds:
          (json['usersIds'] as List<dynamic>).map((e) => e as String).toList(),
      discussionsIds: (json['discussionsIds'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      bookId: json['bookId'] as String,
      startDate:
          const TimestampConverter().fromJson(json['startDate'] as Timestamp),
      endDate: _$JsonConverterFromJson<Timestamp, DateTime>(
          json['endDate'], const TimestampConverter().fromJson),
      views: json['views'] as int? ?? 0,
    );

Map<String, dynamic> _$CreateReadingPlanToJson(CreateReadingPlan instance) =>
    <String, dynamic>{
      'title': instance.title,
      'usersIds': instance.usersIds,
      'discussionsIds': instance.discussionsIds,
      'bookId': instance.bookId,
      'startDate': const TimestampConverter().toJson(instance.startDate),
      'endDate': _$JsonConverterToJson<Timestamp, DateTime>(
          instance.endDate, const TimestampConverter().toJson),
      'views': instance.views,
    };
