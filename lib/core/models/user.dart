import 'package:bookfriends/core/models/timestamp_converter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  final String id;
  final String firstName;
  final String lastName;
  final String? imageUrl;
  final String email;
  @TimestampConverter()
  final DateTime registrationDate;
  @TimestampConverter()
  final DateTime birthDate;
  final List<String> friendsIds;

  const User(
      {required this.id,
      required this.firstName,
      required this.lastName,
      required this.imageUrl,
      required this.registrationDate,
      required this.email,
      required this.birthDate,
      required this.friendsIds});

  factory User.fromSnapshot(DocumentSnapshot<Map<String, dynamic>> snapshot) =>
      _$UserFromJson({
        ...?snapshot.data(),
        'id': snapshot.id,
      });

  Map<String, dynamic> toJson() => _$UserToJson(this);

  // Getters
  String get name => '$firstName $lastName';
}

@JsonSerializable()
class CreateUser {
  final String firstName;
  final String lastName;
  final String? imageUrl;
  final String email;
  @TimestampConverter()
  final DateTime registrationDate;
  @TimestampConverter()
  final DateTime birthDate;
  final List<String> friendsIds;

  const CreateUser(
      {required this.firstName,
      required this.lastName,
      this.imageUrl,
      required this.registrationDate,
      required this.email,
      required this.birthDate,
      required this.friendsIds});

  Map<String, dynamic> toJson() => _$CreateUserToJson(this);
}

final nullUser = User(
    id: '?',
    firstName: 'UserUnavailable',
    lastName: 'UserUnavailable',
    imageUrl: '?',
    registrationDate: DateTime.now(),
    email: 'noEmail',
    birthDate: DateTime.now(),
    friendsIds: []);
