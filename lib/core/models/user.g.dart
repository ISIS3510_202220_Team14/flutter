// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      id: json['id'] as String,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      imageUrl: json['imageUrl'] as String?,
      registrationDate: const TimestampConverter()
          .fromJson(json['registrationDate'] as Timestamp),
      email: json['email'] as String,
      birthDate:
          const TimestampConverter().fromJson(json['birthDate'] as Timestamp),
      friendsIds: (json['friendsIds'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'imageUrl': instance.imageUrl,
      'email': instance.email,
      'registrationDate':
          const TimestampConverter().toJson(instance.registrationDate),
      'birthDate': const TimestampConverter().toJson(instance.birthDate),
      'friendsIds': instance.friendsIds,
    };

CreateUser _$CreateUserFromJson(Map<String, dynamic> json) => CreateUser(
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      imageUrl: json['imageUrl'] as String?,
      registrationDate: const TimestampConverter()
          .fromJson(json['registrationDate'] as Timestamp),
      email: json['email'] as String,
      birthDate:
          const TimestampConverter().fromJson(json['birthDate'] as Timestamp),
      friendsIds: (json['friendsIds'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$CreateUserToJson(CreateUser instance) =>
    <String, dynamic>{
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'imageUrl': instance.imageUrl,
      'email': instance.email,
      'registrationDate':
          const TimestampConverter().toJson(instance.registrationDate),
      'birthDate': const TimestampConverter().toJson(instance.birthDate),
      'friendsIds': instance.friendsIds,
    };
