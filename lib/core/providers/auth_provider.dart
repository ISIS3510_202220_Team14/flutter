import 'package:bookfriends/ui/utils/toast_helper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/widgets.dart';

class AuthProvider extends ChangeNotifier {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  User? _user;

  bool get isLoggedIn => _user != null;
  User? get currentUser => _user;

  AuthProvider() {
    _init();
  }

  void _init() {
    _auth.authStateChanges().listen((user) {
      _user = user;
      notifyListeners();
    });
  }

  void logIn(BuildContext context, String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      Navigator.of(context).popUntil((route) => route.isFirst);
    } catch (error) {
      String msn = '';
      if (error.toString().contains('user-not-found')) {
        msn = 'The email entered is not registered.';
      } else {
        msn = 'There was a problem logging in.\nTry again later.';
      }
      ToastHelper.showToast(context, msn);
    }
  }

  Future<void> logOut(BuildContext context) async {
    await _auth.signOut();
    Navigator.of(context).popUntil((route) => route.isFirst);
  }

  Future<void> singUp(
      BuildContext context, String email, String password) async {
    await _auth.createUserWithEmailAndPassword(
        email: email, password: password);
    Navigator.of(context).popUntil((route) => route.isFirst);
  }
}
