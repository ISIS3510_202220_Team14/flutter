import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import '../models/book.dart';

class BookProvider extends ChangeNotifier {
  Map<String, Book> _books = {};

  CollectionReference<Map<String, dynamic>> booksCollection =
      FirebaseFirestore.instance.collection('books');

  // Getters
  List<Book> get books => _books.values.toList();

  BookProvider() {
    _init();
  }

  void _init() {
    booksCollection.snapshots().listen((event) {
      final snapshots = event.docs;
      final booksList = snapshots
          .map(
            (snapshot) {
              try {
                final book = Book.fromSnapshot(snapshot);
                return book;
              } catch (_) {
                return null;
              }
            },
          )
          .whereType<Book>()
          .toList();

      _books = {for (final book in booksList) book.id: book};
      notifyListeners();
    });
  }

  Book getBookById(String bookId) {
    return _books[bookId] ?? nullBook;
  }

  Future<void> createBook(String name, String author, String genre,
      int numPages, String imageUrl, String isbn) async {
    await booksCollection.add(CreateBook(
      name: name,
      author: author,
      genre: genre,
      numPages: numPages,
      imageUrl: imageUrl,
      isbn: isbn,
    ).toJson());
  }
}
