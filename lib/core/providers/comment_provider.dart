import 'package:bookfriends/core/providers/discussion_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import '../models/comment.dart';

class CommentProvider extends ChangeNotifier {
  Map<String, Comment> _comments = {};

  CollectionReference<Map<String, dynamic>> commentsCollection =
      FirebaseFirestore.instance.collection('comments');

  // Getters
  List<Comment> get comments => _comments.values.toList();

  CommentProvider() {
    _init();
  }

  void _init() {
    commentsCollection.snapshots().listen((event) {
      final snapshots = event.docs;
      final commentsList = snapshots
          .map(
            (snapshot) {
              try {
                final comment = Comment.fromSnapshot(snapshot);
                return comment;
              } catch (_) {
                return null;
              }
            },
          )
          .whereType<Comment>()
          .toList();

      _comments = {for (final comment in commentsList) comment.id: comment};
      notifyListeners();
    });
  }

  Comment getCommentById(String commentId) {
    return _comments[commentId] ?? nullComment;
  }

  List<Comment> getCommentsByIds(List<String> ids) {
    List<Comment> listComments = [];

    for (final id in ids) {
      listComments.add(getCommentById(id));
    }
    return listComments;
  }

  addComment(String text, String discussionId, String currentUserId) {
    DiscussionProvider discussionProvider = DiscussionProvider();

    commentsCollection
        .add({'text': text, 'userId': currentUserId})
        .then((value) => discussionProvider.addComment(value.id, discussionId))
        .catchError((error) => debugPrint('Failed to add discussion: $error'));
  }
}
