import 'package:bookfriends/core/providers/reading_plan_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import '../models/discussion.dart';

class DiscussionProvider extends ChangeNotifier {
  Map<String, Discussion> _discussions = {};

  CollectionReference<Map<String, dynamic>> discussionsCollection =
      FirebaseFirestore.instance.collection('discussions');

  // Getters
  List<Discussion> get discussions => _discussions.values.toList();

  // Post

  addDiscussion(String topic, String readingPlanId) {
    ReadingPlanProvider readingPlanProvider = ReadingPlanProvider();
    discussionsCollection
        .add({'commentsIds': [], 'topic': topic, 'views': 0})
        .then((value) =>
            readingPlanProvider.addDiscussion(value.id, readingPlanId))
        .catchError((error) => debugPrint('Failed to add discussion: $error'));
  }

  DiscussionProvider() {
    _init();
  }

  void _init() {
    discussionsCollection.snapshots().listen((event) {
      final snapshots = event.docs;
      final discussionsList = snapshots
          .map(
            (snapshot) {
              try {
                final discussion = Discussion.fromSnapshot(snapshot);
                return discussion;
              } catch (_) {
                return null;
              }
            },
          )
          .whereType<Discussion>()
          .toList();

      _discussions = {
        for (final discussion in discussionsList) discussion.id: discussion
      };
      notifyListeners();
    });
  }

  Discussion getDiscussionById(String discussionId) {
    return _discussions[discussionId] ?? nullDiscussion;
  }

  List<Discussion> getDiscussionsByIds(List<String> ids) {
    List<Discussion> listDiscussions = [];

    for (final id in ids) {
      listDiscussions.add(getDiscussionById(id));
    }
    return listDiscussions;
  }

  addComment(String idComment, String discussionId) {
    List<String> comments = getDiscussionById(discussionId).commentsIds;
    comments.add(idComment);
    discussionsCollection.doc(discussionId).update({'commentsIds': comments});
  }

  addView(String discussionId) {
    Discussion _discussion = getDiscussionById(discussionId);
    int views = _discussion.views;
    views = views + 1;
    discussionsCollection.doc(_discussion.id).update({'views': views});
  }
}
