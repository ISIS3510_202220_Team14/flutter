import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

import '../models/note.dart';

@pragma('vm:entry-point')
class NoteProvider extends ChangeNotifier {
  Map<String, Note> _notes = {};

  CollectionReference<Map<String, dynamic>> notesCollection =
      FirebaseFirestore.instance.collection('notes');

  // Getters
  List<Note> get notes => _notes.values.toList();

  NoteProvider() {
    _init();
  }

  void _init() {
    notesCollection
        .orderBy('timestamp', descending: true)
        .snapshots()
        .listen((event) {
      final snapshots = event.docs;
      final notesList = snapshots
          .map(
            (snapshot) {
              try {
                final note = Note.fromSnapshot(snapshot);
                return note;
              } catch (error) {
                debugPrint(error.toString());
                return null;
              }
            },
          )
          .whereType<Note>()
          .toList();

      _notes = {for (final note in notesList) note.id: note};
      notifyListeners();
    });
  }

  List<Note> getNotesByIds(List<String> notesIds) =>
      notesIds.map((noteId) => _notes[noteId]).whereType<Note>().toList();

  Note getNoteById(String noteId) {
    return _notes[noteId] ?? nullNote;
  }

  Future<void> createNote(
      String userId, String bookId, String description, String title) async {
    await notesCollection.add(CreateNote(
      userId: userId,
      bookId: bookId,
      description: description,
      title: title,
      timestamp: DateTime.now(),
    ).toJson());
  }

  List<Note> getNotesByUser(String currentUserId) {
    List<Note> notesUser = [];
    for (var note in notes) {
      if (note.userId == currentUserId) {
        notesUser.add(note);
      }
    }
    return notesUser;
  }
}
