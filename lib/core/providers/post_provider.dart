import 'package:bookfriends/core/event_report.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_isolate/flutter_isolate.dart';

import '../models/post.dart';

@pragma('vm:entry-point')
Future<List<String>> filterBySearch(Map<dynamic, dynamic> search) async {
  debugPrint('dentro del isolate');
  final searchWord = search['word'];
  final Map<String, String> contents = search['contents'];
  // lots of calculations

  List<String> validIds = [];

  for (final String contentKey in contents.keys) {
    final content = contents[contentKey];
    if (content?.toLowerCase().contains(searchWord.toLowerCase()) ?? false) {
      validIds.add(contentKey);
    }
  }

  return validIds;
}

class PostProvider extends ChangeNotifier {
  Map<String, Post> _posts = {};

  CollectionReference<Map<String, dynamic>> postsCollection =
      FirebaseFirestore.instance.collection('posts');

  // Getters
  List<Post> get posts => _posts.values.toList();

  PostProvider() {
    _init();
  }

  void _init() {
    postsCollection
        .orderBy('publicationDate', descending: true)
        .snapshots()
        .listen((event) {
      final snapshots = event.docs;
      final postsList = snapshots
          .map(
            (snapshot) {
              try {
                final post = Post.fromSnapshot(snapshot);
                return post;
              } catch (error) {
                debugPrint(error.toString());
                return null;
              }
            },
          )
          .whereType<Post>()
          .toList();

      _posts = {for (final post in postsList) post.id: post};
      notifyListeners();
    });
  }

  Future<List<String>> searchByWord(String word) async {
    debugPrint('palabra: ');
    debugPrint(word);

    final result = await flutterCompute(filterBySearch, {
      'word': word,
      'contents': _posts.map((key, value) => MapEntry(key, value.content)),
    });

    sendFeatureEvent('search_feature');

    return result;
  }

  List<Post> getPostsByIds(List<String> postIds) =>
      postIds.map((postId) => _posts[postId]).whereType<Post>().toList();

  Post getPostById(String postId) {
    return _posts[postId] ?? nullPost;
  }

  Future<void> createPost(
      String userId, String content, String readingPlanId) async {
    await postsCollection.add(CreatePost(
      userId: userId,
      content: content,
      readingPlanId: readingPlanId,
      publicationDate: DateTime.now(),
    ).toJson());
  }
}
