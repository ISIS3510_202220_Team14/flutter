import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';

import '../models/reading_plan.dart';

class ReadingPlanProvider extends ChangeNotifier {
  Map<String, ReadingPlan> _readingPlans = {};

  CollectionReference<Map<String, dynamic>> readingPlansCollection =
      FirebaseFirestore.instance.collection('readingPlans');

  // Getters
  List<ReadingPlan> get readingPlans => _readingPlans.values.toList();

  ReadingPlanProvider() {
    _init();
  }

  void _init() {
    readingPlansCollection.snapshots().listen((event) {
      final snapshots = event.docs;
      final readingPlansList = snapshots
          .map(
            (snapshot) {
              try {
                final readingPlan = ReadingPlan.fromSnapshot(snapshot);
                return readingPlan;
              } catch (_) {
                return null;
              }
            },
          )
          .whereType<ReadingPlan>()
          .toList();

      _readingPlans = {for (final readingPlan in readingPlansList) readingPlan.id: readingPlan};
      notifyListeners();
    });
  }

  ReadingPlan getReadingPlanById(String readingPlanId) {
    return _readingPlans[readingPlanId] ?? nullReadingPlan;
  }

  List<ReadingPlan> getReadingPlansByUserId(String userId) {
    return readingPlans.where((element) => element.usersIds.contains(userId)).toList();
  }

  addDiscussion(String idDiscussion, String readingPlanId) {
    List<String> discussions = getReadingPlanById(readingPlanId).discussionsIds;
    discussions.add(idDiscussion);
    readingPlansCollection.doc(readingPlanId).update({'discussionsIds': discussions});
  }

  addView(String readingPlanId) {
    ReadingPlan _readingPlan = getReadingPlanById(readingPlanId);
    int views = _readingPlan.views;
    views = views + 1;
    readingPlansCollection.doc(_readingPlan.id).update({'views': views});
  }

  Future<void> createReadingPlan({
    required String title,
    required List<String> usersIds,
    List<String> discussionsIds = const [],
    required String bookId,
    required DateTime endDate,
  }) async {
    await readingPlansCollection.add(CreateReadingPlan(
      title: title,
      usersIds: usersIds,
      bookId: bookId,
      endDate: endDate,
      startDate: DateTime.now(),
      discussionsIds: discussionsIds,
    ).toJson());
  }
}
