import 'package:bookfriends/theme/theme_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:shared_preferences/shared_preferences.dart';

//Op1: Cambia el tema
class ThemeChanger with ChangeNotifier {
  static const themePrefKey = 'theme';

  final SharedPreferences _prefs;

  ThemeData _themeData = (ThemeMode.system == ThemeMode.dark)
      ? MyThemes.darkTheme
      : MyThemes.lightTheme;

  ThemeChanger(this._prefs) {
    var loadTheme = _prefs.getString(themePrefKey);
    if (loadTheme == 'dark') {
      _themeData = MyThemes.darkTheme;
    }
    if (loadTheme == 'light') {
      _themeData = MyThemes.lightTheme;
    }
  }

  getTheme() => _themeData;

  setTheme(ThemeData theme) {
    _themeData = theme;
    _prefs.setString(
        themePrefKey, (theme == MyThemes.darkTheme) ? 'dark' : 'light');
    notifyListeners();
  }
}

//Op2: Cambia el modo (light - dark)
class ThemeProvider extends ChangeNotifier {
  ThemeMode themeMode = ThemeMode.system;

  ThemeData themeData = MyThemes.darkTheme;

  ThemeProvider() {
    _init();
  }

  void _init() {
    themeMode = ThemeMode.system;
  }

  bool get isDarkMode {
    if (themeMode == ThemeMode.system) {
      final brightness = SchedulerBinding.instance.window.platformBrightness;
      return brightness == Brightness.dark;
    } else {
      return themeMode == ThemeMode.dark;
    }
  }

  void toggleTheme(bool isOn) {
    themeMode = isOn ? ThemeMode.dark : ThemeMode.light;
    notifyListeners();
  }

  getTheme() => themeData;

  setTheme(ThemeData theme) {
    themeData = theme;
    notifyListeners();
  }
}
