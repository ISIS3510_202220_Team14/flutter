import 'package:bookfriends/core/event_report.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_isolate/flutter_isolate.dart';
import '../models/user.dart';

@pragma('vm:entry-point')
Future<List<String>> filterBySearch(Map<dynamic, dynamic> search) async {
  debugPrint('dentro del isolate');
  final searchWord = search['word'];
  final Map<String, String> contents = search['contents'];
  // lots of calculations

  List<String> validIds = [];

  for (final String contentKey in contents.keys) {
    final content = contents[contentKey];
    if (content?.toLowerCase().contains(searchWord.toLowerCase()) ?? false) {
      validIds.add(contentKey);
    }
  }

  return validIds;
}

class UserProvider extends ChangeNotifier {
  Map<String, User> _users = {};

  CollectionReference<Map<String, dynamic>> usersCollection = FirebaseFirestore.instance.collection('users');

  // Getters
  List<User> get users => _users.values.toList();

  UserProvider() {
    _init();
  }

  void _init() {
    usersCollection.snapshots().listen((event) {
      final snapshots = event.docs;
      final usersList = snapshots
          .map(
            (snapshot) {
              try {
                final user = User.fromSnapshot(snapshot);
                return user;
              } catch (_) {
                return null;
              }
            },
          )
          .whereType<User>()
          .toList();

      _users = {for (final user in usersList) user.id: user};
      notifyListeners();
    });
  }

  User getUserById(String userId) {
    return _users[userId] ?? nullUser;
  }

  User getUserByEmail(String? email) {
    return users.firstWhere(
      (element) => element.email == email,
      orElse: () => nullUser,
    );
  }

  List<User> getUsersByIds(List<String> ids) {
    List<User> listUsers = [];

    for (final id in ids) {
      listUsers.add(getUserById(id));
    }
    return listUsers;
  }

  Future<void> createUser(String firstName, String lastName, String email, DateTime birthDate) async {
    await usersCollection.add(CreateUser(
        firstName: firstName,
        lastName: lastName,
        email: email,
        registrationDate: DateTime.now(),
        birthDate: birthDate,
        friendsIds: []).toJson());
  }

  Future<void> addFriend(User currentUser, String friendId) async {
    List<String> friends = currentUser.friendsIds;
    if (!friends.contains(friendId) && currentUser.id != friendId) {
      friends.add(friendId);
      usersCollection.doc(currentUser.id).update({'friendsIds': friends});
    }
  }

  Future<List<String>> searchUsersByName(String searchName) async {
    final result = await flutterCompute(filterBySearch, {
      'word': searchName,
      'contents': _users.map((key, value) => MapEntry(key, value.name)),
    });

    sendFeatureEvent('search_feature');

    return result;
  }
}
