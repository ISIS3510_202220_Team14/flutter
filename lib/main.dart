import 'dart:core';
import 'package:bookfriends/core/providers/auth_provider.dart';
import 'package:bookfriends/core/providers/book_provider.dart';
import 'package:bookfriends/core/providers/comment_provider.dart';
import 'package:bookfriends/core/providers/discussion_provider.dart';
import 'package:bookfriends/core/providers/note_provider.dart';
import 'package:bookfriends/core/providers/reading_plan_provider.dart';
import 'package:bookfriends/core/providers/user_provider.dart';
import 'package:bookfriends/ui/screens/Welcome/welcome_screen.dart';
import 'package:bookfriends/ui/screens/home_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'core/providers/post_provider.dart';
import 'firebase_options.dart';
import 'package:bookfriends/core/providers/theme_provider.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();

  debugPrint('Handling a background message: ${message.messageId}');
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  try {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
  } catch (error) {
    debugPrint('Firebase initialization failed');
    debugPrint(error.toString());
  }
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  NotificationSettings settings = await messaging.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );

  debugPrint('User granted permission: ${settings.authorizationStatus}');

  FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    debugPrint('Got a message whilst in the foreground!');
    debugPrint('Message data: ${message.data}');

    if (message.notification != null) {
      debugPrint(
          'Message also contained a notification: ${message.notification}');
    }
  });

  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  final prefs = await SharedPreferences.getInstance();
  runApp(MyApp(prefs: prefs));
}

class MyApp extends StatelessWidget {
  final SharedPreferences prefs;
  const MyApp({required this.prefs, super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ThemeChanger(prefs),
      child: const MaterialAppWithTheme(),
    );
  }
}

class MaterialAppWithTheme extends StatelessWidget {
  const MaterialAppWithTheme({Key? key}) : super(key: key);

  Widget _buildApp() {
    return Selector<AuthProvider, bool>(
      selector: (_, authProvider) => authProvider.isLoggedIn,
      builder: (context, isLoggedIn, _) {
        return MultiProvider(
          providers: [
            ChangeNotifierProvider<BookProvider>(
              create: (_) => BookProvider(),
              lazy: false,
            ),
            ChangeNotifierProvider<UserProvider>(
              create: (_) => UserProvider(),
              lazy: false,
            ),
            ChangeNotifierProvider<ReadingPlanProvider>(
              create: (_) => ReadingPlanProvider(),
              lazy: false,
            ),
            ChangeNotifierProvider<PostProvider>(
              create: (_) => PostProvider(),
              lazy: false,
            ),
            ChangeNotifierProvider<DiscussionProvider>(
              create: (_) => DiscussionProvider(),
              lazy: false,
            ),
            ChangeNotifierProvider<CommentProvider>(
              create: (_) => CommentProvider(),
              lazy: false,
            ),
            ChangeNotifierProvider<NoteProvider>(
              create: (_) => NoteProvider(),
              lazy: false,
            ),
          ],
          child: Consumer<ThemeChanger>(
              builder: (context, ThemeChanger themeNotifier, child) {
            return MaterialApp(
              title: 'BookFriends',
              theme: themeNotifier.getTheme(),
              home: isLoggedIn ? const HomeScreen() : const WelcomeScreen(),
            );
          }),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AuthProvider>(
      create: (_) => AuthProvider(),
      lazy: false,
      child: _buildApp(),
    );
  }
}

extension FirestoreDocumentExtension<T> on DocumentReference<T> {
  Future<DocumentSnapshot<T>> getCacheFirst() async {
    try {
      var ds = await get(const GetOptions(source: Source.cache));
      if (!ds.exists) return get(const GetOptions(source: Source.server));
      return ds;
    } catch (_) {
      return get(const GetOptions(source: Source.server));
    }
  }
}

extension FirestoreQueryExtension<T> on Query<T> {
  Future<QuerySnapshot<T>> getCacheFirst() async {
    try {
      var qs = await get(const GetOptions(source: Source.cache));
      if (qs.docs.isEmpty) return get(const GetOptions(source: Source.server));
      return qs;
    } catch (_) {
      return get(const GetOptions(source: Source.server));
    }
  }
}
