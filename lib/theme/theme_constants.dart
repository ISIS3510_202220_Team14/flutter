import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const Color socialPink = Color(0xFFF62E8E);
const Color socialBlue = Color(0xFF2E8AF6);
const Color darkBlack = Color(0xFF181A1C);
const Color grey = Color(0xFF323436);
const Color lightGrey = Color(0xFF727477);
const Color lightWhite = Color(0xFFECEBED);
const Color kPrimaryColor = Color(0xFF6F35A5);
const Color kPrimaryLightColor = Color(0xFFF1E6FF);
const double defaultPadding = 16.0;

class MyThemes {
  static final lightTheme = ThemeData(
    brightness: Brightness.light,
    colorScheme: const ColorScheme.light(),
    scaffoldBackgroundColor: Colors.white,
    fontFamily: GoogleFonts.alegreya().fontFamily,
    textTheme: const TextTheme(bodyText2: TextStyle(color: darkBlack)),
    iconTheme: const IconThemeData(color: darkBlack),
    primaryColor: Colors.white,
  );

  static final darkTheme = ThemeData(
    brightness: Brightness.dark,
    colorScheme: const ColorScheme.dark(),
    scaffoldBackgroundColor: darkBlack,
    fontFamily: GoogleFonts.alegreya().fontFamily,
    textTheme: const TextTheme(bodyText2: TextStyle(color: lightWhite)),
    iconTheme: const IconThemeData(color: lightWhite),
    primaryColor: Colors.black,
  );
}
