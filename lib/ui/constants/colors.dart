import 'package:flutter/material.dart';

class BookFriendsColor {
  static const Color socialPink = Color(0xFFF62E8E);
  static const Color socialBlue = Color(0xFF2E8AF6);
  static const Color darkBlack = Color(0xFF181A1C);
  static const Color grey = Color(0xFF323436);
  static const Color lightGrey = Color(0xFF727477);
  static const Color lightWhite = Color(0xFFECEBED);
}
