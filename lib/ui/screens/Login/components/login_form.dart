import 'package:bookfriends/core/providers/auth_provider.dart';
import 'package:bookfriends/theme/theme_constants.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';
import '../../../components/already_have_an_account_acheck.dart';
import '../../Signup/signup_screen.dart';

class LoginForm extends StatelessWidget {
  LoginForm({
    Key? key,
  }) : super(key: key);

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    checkConnection() async {
      bool result = await InternetConnectionChecker().hasConnection;
      if (result == true) {
        String email = emailController.text;
        String password = passwordController.text;
        String msn = '';
        if (email.isNotEmpty && EmailValidator.validate(email)) {
          if (password.isNotEmpty) {
            try {
              final authProvider = context.read<AuthProvider>();
              authProvider.logIn(context, email, password);
            } catch (error) {
              msn = error.toString();
              var index = msn.indexOf(']');
              msn = msn.substring(index + 2);
            }
          } else {
            msn = 'Enter your password.';
          }
        } else {
          msn = 'Enter a valid email:\nexample@example.com';
        }
        if (msn != '') {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(msn),
            ),
          );
        }
      } else {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('There is no Internet Connection. Try again Later'),
          ),
        );
      }
    }

    return Form(
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                color: kPrimaryLightColor,
                borderRadius: BorderRadius.circular(29)),
            child: TextFormField(
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              style: const TextStyle(color: kPrimaryColor),
              cursorColor: kPrimaryColor,
              onSaved: (email) {},
              maxLength: 30,
              decoration: const InputDecoration(
                counterText: '',
                hintText: 'Your email',
                hintStyle: TextStyle(color: kPrimaryColor),
                border: InputBorder.none,
                prefixIcon: Padding(
                  padding: EdgeInsets.all(defaultPadding),
                  child: Icon(
                    Icons.person,
                    color: kPrimaryColor,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defaultPadding),
            child: Container(
              decoration: BoxDecoration(
                  color: kPrimaryLightColor,
                  borderRadius: BorderRadius.circular(29)),
              child: TextFormField(
                controller: passwordController,
                textInputAction: TextInputAction.done,
                style: const TextStyle(color: kPrimaryColor),
                obscureText: true,
                cursorColor: kPrimaryColor,
                maxLength: 20,
                decoration: const InputDecoration(
                  counterText: '',
                  hintText: 'Your password',
                  hintStyle: TextStyle(color: kPrimaryColor),
                  border: InputBorder.none,
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Icon(
                      Icons.lock,
                      color: kPrimaryColor,
                    ),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: defaultPadding),
          Hero(
            tag: 'login_btn',
            child: ElevatedButton(
              onPressed: () {
                checkConnection();
              },
              style: ElevatedButton.styleFrom(backgroundColor: socialPink),
              child: Text(
                'Login'.toUpperCase(),
              ),
            ),
          ),
          const SizedBox(height: defaultPadding),
          AlreadyHaveAnAccountCheck(
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return const SignUpScreen();
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
