import 'package:bookfriends/theme/theme_constants.dart';
import 'package:bookfriends/core/providers/user_provider.dart';
import 'package:bookfriends/ui/utils/toast_helper.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:bookfriends/core/providers/auth_provider.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';
import '../../../components/already_have_an_account_acheck.dart';
import '../../Login/login_screen.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({
    Key? key,
  }) : super(key: key);

  @override
  State<SignUpForm> createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  DateTime? _selectedDateTime;
  void _signUp(BuildContext context) async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      String name = _firstNameController.text;
      String lastName = _lastNameController.text;
      String email = _emailController.text;
      String password = _passwordController.text;
      String password2 = _confirmPasswordController.text;
      String msn = '';
      if (name.isNotEmpty) {
        if (lastName.isNotEmpty) {
          if (EmailValidator.validate(email)) {
            if (password.length >= 6) {
              if (password == password2) {
                if (_selectedDateTime != null && (DateTime.now().year - _selectedDateTime!.year > 8)) {
                  final authProvider = context.read<AuthProvider>();
                  try {
                    await authProvider.singUp(context, email, password);
                    await context.read<UserProvider>().createUser(
                        _firstNameController.text, _lastNameController.text, _emailController.text, _selectedDateTime!);
                  } catch (error) {
                    String msn = error.toString();
                    var index = msn.indexOf(']');
                    msn = msn.substring(index + 2);
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(msn),
                      ),
                    );
                  }
                } else {
                  msn = 'Please enter a valid birth date';
                }
              } else {
                msn = 'The passwords do not match.';
              }
            } else {
              msn = 'Password must have at least 6 characters.';
            }
          } else {
            msn = 'Enter a valid email:\nexample@example.com';
          }
        } else {
          msn = 'Last names cannot be empty.';
        }
      } else {
        msn = 'Names cannot be empty.';
      }
      if (msn != '') {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(msn),
          ),
        );
      }
    } else {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('There is no Internet Connection. Try again Later'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defaultPadding),
            child: Container(
              decoration: BoxDecoration(color: kPrimaryLightColor, borderRadius: BorderRadius.circular(29)),
              child: TextFormField(
                textInputAction: TextInputAction.next,
                style: const TextStyle(color: kPrimaryColor),
                cursorColor: kPrimaryColor,
                controller: _firstNameController,
                maxLength: 30,
                decoration: const InputDecoration(
                  counterText: '',
                  hintText: 'Write your First Name',
                  hintStyle: TextStyle(color: kPrimaryColor),
                  border: InputBorder.none,
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defaultPadding),
            child: Container(
              decoration: BoxDecoration(color: kPrimaryLightColor, borderRadius: BorderRadius.circular(29)),
              child: TextFormField(
                textInputAction: TextInputAction.next,
                style: const TextStyle(color: kPrimaryColor),
                cursorColor: kPrimaryColor,
                controller: _lastNameController,
                maxLength: 30,
                decoration: const InputDecoration(
                  counterText: '',
                  hintText: 'Write your Last Name',
                  hintStyle: TextStyle(color: kPrimaryColor),
                  border: InputBorder.none,
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defaultPadding),
            child: Container(
              decoration: BoxDecoration(color: kPrimaryLightColor, borderRadius: BorderRadius.circular(29)),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                style: const TextStyle(color: kPrimaryColor),
                cursorColor: kPrimaryColor,
                controller: _emailController,
                maxLength: 30,
                decoration: const InputDecoration(
                  counterText: '',
                  hintText: 'Your email',
                  hintStyle: TextStyle(color: kPrimaryColor),
                  border: InputBorder.none,
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Icon(color: kPrimaryColor, Icons.person),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defaultPadding),
            child: Container(
              decoration: BoxDecoration(color: kPrimaryLightColor, borderRadius: BorderRadius.circular(29)),
              child: TextFormField(
                textInputAction: TextInputAction.done,
                style: const TextStyle(color: kPrimaryColor),
                obscureText: true,
                cursorColor: kPrimaryColor,
                controller: _passwordController,
                maxLength: 20,
                decoration: const InputDecoration(
                  counterText: '',
                  hintText: 'Your password',
                  hintStyle: TextStyle(color: kPrimaryColor),
                  border: InputBorder.none,
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Icon(color: kPrimaryColor, Icons.lock),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defaultPadding),
            child: Container(
              decoration: BoxDecoration(
                color: kPrimaryLightColor,
                borderRadius: BorderRadius.circular(29),
              ),
              child: TextFormField(
                textInputAction: TextInputAction.done,
                style: const TextStyle(color: kPrimaryColor),
                obscureText: true,
                cursorColor: kPrimaryColor,
                controller: _confirmPasswordController,
                maxLength: 20,
                decoration: const InputDecoration(
                  counterText: '',
                  hintText: 'Confirm Your password',
                  hintStyle: TextStyle(color: kPrimaryColor),
                  border: InputBorder.none,
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Icon(color: kPrimaryColor, Icons.lock),
                  ),
                ),
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: defaultPadding),
              child: SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.resolveWith(
                        (_) => RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(29),
                        ),
                      ),
                    ),
                    onPressed: () async {
                      final selectedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1900),
                        lastDate: DateTime.now(),
                        initialDatePickerMode: DatePickerMode.year,
                      );

                      setState(() {
                        _selectedDateTime = selectedDate;
                      });
                    },
                    child: Text(
                      _selectedDateTime != null ? '${_selectedDateTime?.toIso8601String()}' : 'Insert your birthdate',
                    )),
              )),
          const SizedBox(height: defaultPadding / 2),
          ElevatedButton(
            onPressed: () => _signUp(context),
            style: ElevatedButton.styleFrom(backgroundColor: socialPink),
            child: Text('Sign Up'.toUpperCase()),
          ),
          const SizedBox(height: defaultPadding),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: defaultPadding),
            child: AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const LoginScreen();
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
