import 'package:bookfriends/theme/theme_constants.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../../Login/login_screen.dart';
import '../../Signup/signup_screen.dart';

class LoginAndSignupBtn extends StatefulWidget {
  const LoginAndSignupBtn({Key? key}) : super(key: key);

  @override
  LoginAndSignupBtState createState() => LoginAndSignupBtState();
}

class LoginAndSignupBtState extends State<LoginAndSignupBtn> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Hero(
          tag: 'login_btn',
          child: ElevatedButton(
            onPressed: () {
              _checkConnection('login');
            },
            style: ElevatedButton.styleFrom(backgroundColor: socialPink),
            child: Text(
              'Login'.toUpperCase(),
            ),
          ),
        ),
        const SizedBox(width: 16),
        ElevatedButton(
          onPressed: () {
            _checkConnection('singup');
          },
          style: ElevatedButton.styleFrom(
              backgroundColor: lightWhite, elevation: 0),
          child: Text(
            'Sign Up'.toUpperCase(),
            style: const TextStyle(color: Colors.black),
          ),
        ),
      ],
    );
  }

  _checkConnection(String action) async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      if (action == 'login') {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return const LoginScreen();
            },
          ),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return const SignUpScreen();
            },
          ),
        );
      }
    } else {
      String msn = '';
      if (action == 'login') {
        msn = 'There is no Internet connection.\nTry logging in later';
      } else {
        msn = 'There is no Internet connection.\nTry signing up later';
      }
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msn),
      ));
    }
  }
}
