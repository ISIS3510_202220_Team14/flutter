import 'package:flutter/material.dart';

import '../../constants.dart';

class WelcomeImage extends StatelessWidget {
  const WelcomeImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset(
          'assets/images/logo_nofondo.png',
          width: MediaQuery.of(context).size.width * 0.6,
        ),
        const SizedBox(height: defaultPadding * 4),
        const Text(
          'ReadBooks',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 22.0,
          ),
        ),
        const SizedBox(height: defaultPadding * 4),
      ],
    );
  }
}
