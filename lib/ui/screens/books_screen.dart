import 'package:bookfriends/core/models/book.dart';
import 'package:bookfriends/core/providers/book_provider.dart';
import 'package:bookfriends/core/providers/theme_provider.dart';
import 'package:bookfriends/ui/constants/colors.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BooksScreen extends StatefulWidget {
  const BooksScreen({super.key});

  @override
  State<BooksScreen> createState() => _BooksScreen();
}

class _BooksScreen extends State<BooksScreen> {
  Widget _buildHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: const [
        Text(
          'Books',
          style: TextStyle(
            color: Colors.white,
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget _sectionTitle(title) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(4, 4, 4, 12),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          title,
          style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }

  Widget _bookContent<Book>(book) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 184,
          child: Stack(
            children: [
              Positioned.fill(
                child: CachedNetworkImage(
                  imageUrl: book.imageUrl,
                  fit: BoxFit.cover,
                  placeholder: (context, url) =>
                      Image.asset('assets/images/no_book.jpg'),
                  errorWidget: (context, url, error) =>
                      Image.asset('assets/images/no_book.jpg'),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: DefaultTextStyle(
            softWrap: false,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  book.author,
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 8.0,
                ),
                Text(
                  book.genre,
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                const SizedBox(
                  height: 8.0,
                ),
                Text(
                  '${book.numPages} pages',
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                const SizedBox(
                  height: 8.0,
                ),
                Text(
                  'ISBN: ${book.isbn}',
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                const SizedBox(
                  height: 8.0,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        appBar: AppBar(
            backgroundColor: BookFriendsColor.darkBlack,
            title: _buildHeader(),
            centerTitle: true),
        body: Scrollbar(
          child: Selector<BookProvider, List<Book>>(
            selector: (_, bookProvider) {
              return bookProvider.books;
            },
            builder: (context, books, child) => ListView(
                restorationId: 'books_list_view',
                padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
                children: [
                  for (final book in books)
                    Container(
                      margin: const EdgeInsets.only(bottom: 8),
                      child: SafeArea(
                          top: false,
                          bottom: false,
                          child: Padding(
                            padding: const EdgeInsets.all(8),
                            child: Column(children: [
                              _sectionTitle(book.name),
                              SizedBox(
                                height: 360.0,
                                child: Card(
                                  // This ensures that the Card's children are clipped correctly.
                                  clipBehavior: Clip.antiAlias,
                                  child: _bookContent(book),
                                ),
                              ),
                            ]),
                          )),
                    )
                ]),
          ),
        ));
  }
}
