import 'package:bookfriends/core/providers/book_provider.dart';
import 'package:bookfriends/ui/constants/colors.dart';
import 'package:bookfriends/ui/utils/toast_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';
import 'package:shake/shake.dart';

class CreateBookScreen extends StatefulWidget {
  const CreateBookScreen({super.key});

  @override
  State<CreateBookScreen> createState() => _CreateBookScreen();
}

class _CreateBookScreen extends State<CreateBookScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _textAuthor = TextEditingController();
  final TextEditingController _textGenre = TextEditingController();
  final TextEditingController _textImageUrl = TextEditingController();
  final TextEditingController _textISBN = TextEditingController();
  final TextEditingController _textName = TextEditingController();
  final TextEditingController _textNumberPags = TextEditingController();

  late ShakeDetector shakeDetector;

  @override
  void initState() {
    super.initState();
    shakeDetector = ShakeDetector.autoStart(onPhoneShake: () {
      setState(() {
        _textName.clear();
        _textAuthor.clear();
        _textGenre.clear();
        _textISBN.clear();
        _textImageUrl.clear();
        _textNumberPags.clear();
      });
      print('Se mueveeee');
    });
  }

  Widget _buildHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: const [
        Text(
          'Create a new book',
          style: TextStyle(
            color: Colors.white,
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget _info() {
    return Container(
      padding: const EdgeInsets.all(10.00),
      margin: const EdgeInsets.only(bottom: 10.00),
      color: Colors.lightGreenAccent.shade100,
      child: Row(children: [
        Container(
          margin: const EdgeInsets.only(right: 6.00),
          child: const Icon(Icons.info, color: Colors.white),
        ), // icon for error message

        const Text('Shake to clear all fields',
            style: TextStyle(color: Colors.black)),
        //show error message text
      ]),
    );
  }

  Widget _buildTextFieldName() {
    return TextFormField(
      obscureText: false,
      controller: _textName,
      maxLines: 1,
      maxLength: 40,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: 'Name',
      ),
      validator: ((value) {
        if (value == null || value.isEmpty) {
          return 'Please enter some text';
        }
        return null;
      }),
    );
  }

  Widget _buildTextFieldAuthor() {
    return TextFormField(
      obscureText: false,
      controller: _textAuthor,
      maxLines: 1,
      maxLength: 40,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: 'Author',
      ),
      validator: ((value) {
        if (value == null || value.isEmpty) {
          return 'Please enter some text';
        }
        return null;
      }),
    );
  }

  Widget _buildTextFieldGenre() {
    return TextFormField(
      obscureText: false,
      controller: _textGenre,
      maxLines: 1,
      maxLength: 30,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: 'Genre',
      ),
      validator: ((value) {
        if (value == null || value.isEmpty) {
          return 'Please enter some text';
        }
        return null;
      }),
    );
  }

  Widget _buildTextFieldNumPags() {
    return TextFormField(
      obscureText: false,
      keyboardType: TextInputType.number,
      controller: _textNumberPags,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      maxLines: 1,
      maxLength: 4,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: 'Number of Pages',
      ),
      validator: ((value) {
        if (value == null || value.isEmpty) {
          return 'Please enter this field';
        }
        return null;
      }),
    );
  }

  Widget _buildTextFieldImageUrl() {
    return TextFormField(
      obscureText: false,
      controller: _textImageUrl,
      maxLines: 1,
      maxLength: 200,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: 'Image URL',
      ),
      validator: ((value) {
        if (value == null || value.isEmpty) {
          return 'Please enter some text';
        }
        if (!Uri.parse(value).isAbsolute) {
          return 'This is not a URL.';
        }
        return null;
      }),
    );
  }

  Widget _buildTextFieldISBN() {
    return TextFormField(
      obscureText: false,
      controller: _textISBN,
      maxLines: 1,
      maxLength: 40,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: 'ISBN',
      ),
      validator: ((value) {
        if (value == null || value.isEmpty) {
          return 'Please enter some text';
        }
        return null;
      }),
    );
  }

  Widget _buildCreateButton(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        _checkConnectionInput();
      },
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith(
            (_) => BookFriendsColor.socialPink),
      ),
      child: const Text(
        'Create book',
        style: TextStyle(
          color: Colors.white,
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
          backgroundColor: BookFriendsColor.darkBlack,
          title: _buildHeader(),
          centerTitle: true),
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: SafeArea(
          child: Center(
            child: Form(
              key: _formKey,
              child: ListView(
                physics: const BouncingScrollPhysics(),
                shrinkWrap: true,
                padding: const EdgeInsets.symmetric(
                  vertical: 32.0,
                  horizontal: 48.0,
                ),
                children: [
                  _info(),
                  _buildTextFieldName(),
                  const SizedBox(height: 16.0),
                  _buildTextFieldAuthor(),
                  const SizedBox(height: 16.0),
                  _buildTextFieldGenre(),
                  const SizedBox(height: 16.0),
                  _buildTextFieldNumPags(),
                  const SizedBox(height: 16.0),
                  _buildTextFieldImageUrl(),
                  const SizedBox(height: 16.0),
                  _buildTextFieldISBN(),
                  const SizedBox(height: 16.0),
                  _buildCreateButton(context)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _checkConnectionInput() async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      if (_formKey.currentState!.validate()) {
        try {
          final bookProvider = context.read<BookProvider>();
          bookProvider.createBook(
              _textName.text,
              _textAuthor.text,
              _textGenre.text,
              int.parse(_textNumberPags.text),
              _textImageUrl.text,
              _textISBN.text);

          setState(() {
            _textName.clear();
            _textAuthor.clear();
            _textGenre.clear();
            _textISBN.clear();
            _textImageUrl.clear();
            _textNumberPags.clear();
          });

          ToastHelper.showToast(context, 'Book created');
        } catch (error) {
          ToastHelper.showToast(context, 'Book could not be created');
        }
      }
    } else {
      ToastHelper.showToast(
          context, 'Book not created, Please check your connection.');
    }
  }
}
