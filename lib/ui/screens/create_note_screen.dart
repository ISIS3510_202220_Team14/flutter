import 'package:bookfriends/core/models/book.dart';
import 'package:bookfriends/core/providers/auth_provider.dart';
import 'package:bookfriends/core/providers/book_provider.dart';
import 'package:bookfriends/core/providers/note_provider.dart';
import 'package:bookfriends/core/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';
import '../constants/colors.dart';

class CreateNoteScreen extends StatefulWidget {
  const CreateNoteScreen({super.key});

  @override
  State<CreateNoteScreen> createState() => _CreateNoteScreen();
}

class _CreateNoteScreen extends State<CreateNoteScreen> {
  String? _selectedBookId;
  final TextEditingController _textTitle = TextEditingController();
  final TextEditingController _textDescription = TextEditingController();

  Widget _buildHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: const [
        Text(
          'Create a new Note',
          style: TextStyle(
            color: Colors.white,
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget _buildTextFieldTitle() {
    return TextFormField(
      obscureText: false,
      controller: _textTitle,
      maxLines: 1,
      maxLength: 40,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: 'Title',
      ),
      validator: ((value) {
        if (value == null || value.isEmpty) {
          return 'Please enter some text';
        }
        return null;
      }),
    );
  }

  Widget _buildTextFieldDescription() {
    return TextFormField(
      obscureText: false,
      controller: _textDescription,
      maxLines: 5,
      maxLength: 300,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: 'Content...',
      ),
    );
  }

  Widget _buildCreateButton(BuildContext context) {
    return ElevatedButton(
      onPressed: _selectedBookId == null
          ? () => _checkConnection(false)
          : () => _checkConnection(true),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith(
            (_) => BookFriendsColor.socialPink),
      ),
      child: const Text(
        'Create Note',
        style: TextStyle(
          color: Colors.white,
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  Widget _buildBookDropDown() {
    return Selector<BookProvider, List<Book>>(
      selector: (_, bookProvider) {
        final books = bookProvider.books;
        return books;
      },
      builder: (context, books, _) => DropdownButton<String>(
        hint: books.isNotEmpty
            ? const Text('Select the book for the new note:')
            : const Text('There are no Books, try creating one first.'),
        isExpanded: true,
        value: _selectedBookId,
        items: books
            .map((book) => DropdownMenuItem<String>(
                  value: book.id,
                  child: Text(book.name),
                ))
            .toList(),
        onChanged: (selectedBookId) {
          setState(() {
            _selectedBookId = selectedBookId;
          });
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
          backgroundColor: BookFriendsColor.darkBlack,
          title: _buildHeader(),
          centerTitle: true),
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: SafeArea(
          child: ListView(
            physics: const BouncingScrollPhysics(),
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(
              vertical: 32.0,
              horizontal: 48.0,
            ),
            children: [
              const SizedBox(height: 16.0),
              _buildBookDropDown(),
              const SizedBox(height: 16.0),
              _buildTextFieldTitle(),
              const SizedBox(height: 16.0),
              _buildTextFieldDescription(),
              const SizedBox(height: 16.0),
              _buildCreateButton(context)
            ],
          ),
        ),
      ),
    );
  }

  _checkConnection(bool ready) async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      if (ready) {
        if (_textTitle.text.isNotEmpty) {
          if (_textDescription.text.isNotEmpty) {
            final noteProvider = context.read<NoteProvider>();
            final userProvider = context.read<UserProvider>();
            final authProvider = context.read<AuthProvider>();

            final emailCurrentUser = authProvider.currentUser?.email.toString();
            final currentUserId =
                userProvider.getUserByEmail(emailCurrentUser).id;
            if (_selectedBookId != null) {
              try {
                await noteProvider.createNote(currentUserId, _selectedBookId!,
                    _textDescription.text, _textTitle.text);

                setState(() {
                  _selectedBookId = null;
                  _textTitle.clear();
                  _textDescription.clear();
                });
                Navigator.of(context).pop();
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Note added')),
                );
              } catch (error) {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Note could not be added')),
                );
              }
            }
            FocusManager.instance.primaryFocus?.unfocus();
          } else {
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                  content: Text('Please enter a content for your note.')),
            );
          }
        } else {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
                content: Text('Please enter some title for your note.')),
          );
        }
      } else {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
              content: Text(
                  'Please select a book. If you do not have any, try creating one first.')),
        );
      }
    } else {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('There is no Internet Connection. Try again Later')),
      );
    }
  }
}
