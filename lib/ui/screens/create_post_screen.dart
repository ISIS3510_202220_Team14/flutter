import 'package:bookfriends/core/providers/auth_provider.dart';
import 'package:bookfriends/core/providers/post_provider.dart';
import 'package:bookfriends/core/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';

import '../../core/models/reading_plan.dart';
import '../../core/providers/reading_plan_provider.dart';
import '../constants/colors.dart';

class CreatePostScreen extends StatefulWidget {
  const CreatePostScreen({super.key});

  @override
  State<CreatePostScreen> createState() => _CreatePostScreen();
}

class _CreatePostScreen extends State<CreatePostScreen> {
  String? _selectedReadingPlanId;
  final TextEditingController _textPost = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  Widget _buildHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: const [
        Text(
          'Create a new post',
          style: TextStyle(
            color: Colors.white,
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget _buildTextField() {
    return TextField(
      key: _formKey,
      obscureText: false,
      controller: _textPost,
      maxLines: 5,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: 'Create your post...',
      ),
    );
  }

  Widget _buildCreateButton(BuildContext context) {
    return ElevatedButton(
      onPressed: _selectedReadingPlanId == null
          ? () => _checkConnection(false)
          : () => _checkConnection(true),
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith(
            (_) => BookFriendsColor.socialPink),
      ),
      child: const Text(
        'Create post',
        style: TextStyle(
          color: Colors.white,
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  Widget _buildReadingPlanDropDown() {
    return Selector3<ReadingPlanProvider, UserProvider, AuthProvider,
        List<ReadingPlan>>(
      selector: (_, readingPlanProvider, userProvider, authProvider) {
        final emailCurrentUser = authProvider.currentUser?.email.toString();
        final currentUserId = userProvider.getUserByEmail(emailCurrentUser).id;
        final userReadingPlans =
            readingPlanProvider.getReadingPlansByUserId(currentUserId);

        return userReadingPlans;
      },
      builder: (context, readingPlans, _) => DropdownButton<String>(
        hint: readingPlans.isNotEmpty
            ? const Text('Select the reading plan for the new post:')
            : const Text('You do not have any reading plan'),
        isExpanded: true,
        value: _selectedReadingPlanId,
        items: readingPlans
            .map((readingPlan) => DropdownMenuItem<String>(
                  value: readingPlan.id,
                  child: Text(readingPlan.title),
                ))
            .toList(),
        onChanged: (selectedReadingPlanId) {
          setState(() {
            _selectedReadingPlanId = selectedReadingPlanId;
          });
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
          backgroundColor: BookFriendsColor.darkBlack,
          title: _buildHeader(),
          centerTitle: true),
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: SafeArea(
          child: Center(
            child: ListView(
              physics: const BouncingScrollPhysics(),
              shrinkWrap: true,
              padding: const EdgeInsets.symmetric(
                vertical: 32.0,
                horizontal: 48.0,
              ),
              children: [
                _buildReadingPlanDropDown(),
                const SizedBox(height: 16.0),
                _buildTextField(),
                const SizedBox(height: 16.0),
                _buildCreateButton(context)
              ],
            ),
          ),
        ),
      ),
    );
  }

  _checkConnection(bool ready) async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      if (ready) {
        if (_textPost.text.isNotEmpty) {
          final postProvider = context.read<PostProvider>();
          final userProvider = context.read<UserProvider>();
          final authProvider = context.read<AuthProvider>();

          final emailCurrentUser = authProvider.currentUser?.email.toString();
          final currentUserId =
              userProvider.getUserByEmail(emailCurrentUser).id;
          if (_selectedReadingPlanId != null) {
            try {
              await postProvider.createPost(
                  currentUserId, _textPost.text, _selectedReadingPlanId!);

              setState(() {
                _selectedReadingPlanId = null;
                _textPost.clear();
              });
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('post created')),
              );
            } catch (error) {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('post could not be created')),
              );
            }
          }
          FocusManager.instance.primaryFocus?.unfocus();
        } else {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
                content: Text('Please enter some text for your post.')),
          );
        }
      } else {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
              content: Text(
                  'Please select a reading plan. If you do not have any, try creating one first.')),
        );
      }
    } else {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('There is no Internet Connection. Try again Later')),
      );
    }
  }
}
