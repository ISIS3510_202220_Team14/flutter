import 'package:bookfriends/core/models/book.dart';
import 'package:bookfriends/core/models/user.dart';
import 'package:bookfriends/core/providers/auth_provider.dart';
import 'package:bookfriends/core/providers/book_provider.dart';
import 'package:bookfriends/core/providers/reading_plan_provider.dart';
import 'package:bookfriends/core/providers/user_provider.dart';
import 'package:bookfriends/ui/constants/colors.dart';
import 'package:bookfriends/ui/widgets/select_user_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateReadingPlanScreen extends StatefulWidget {
  const CreateReadingPlanScreen({super.key});

  @override
  State<CreateReadingPlanScreen> createState() => _CreateReadingPlanScreen();
}

class _CreateReadingPlanScreen extends State<CreateReadingPlanScreen> {
  final TextEditingController _titleController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // Consts
  static const _verticalSpace = SizedBox(height: 16.0);

  // State variables
  final Set<String> _selectedUsersIds = {};
  String? _selectedBookId;
  DateTime? _selectedEndDate;

  @override
  void initState() {
    super.initState();
    final authProvider = context.read<AuthProvider>();
    final userProvider = context.read<UserProvider>();
    final emailCurrentUser = authProvider.currentUser?.email.toString();
    final currentUserId = userProvider.getUserByEmail(emailCurrentUser).id;
    _selectedUsersIds.add(currentUserId);
  }

  Widget _buildHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: const [
        Text(
          'Create a new reading plan',
          style: TextStyle(
            color: Colors.white,
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget _buildTextField({required TextEditingController controller, String? label}) {
    return TextField(
      key: _formKey,
      controller: controller,
      decoration: InputDecoration(
        border: const OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: label,
      ),
    );
  }

  Widget _buildBookDropDown() {
    return Selector<BookProvider, List<Book>>(
      selector: (_, bookProvider) {
        return bookProvider.books;
      },
      builder: (context, books, _) => DropdownButton<String>(
        hint: books.isNotEmpty
            ? const Text('Select the book for the new reading plan:')
            : const Text('There is no book to select'),
        isExpanded: true,
        value: _selectedBookId,
        items: books
            .map((book) => DropdownMenuItem<String>(
                  value: book.id,
                  child: Text(book.name),
                ))
            .toList(),
        onChanged: (selectedBookId) {
          setState(() {
            _selectedBookId = selectedBookId;
          });
        },
      ),
    );
  }

  Widget _buildSelectedUsersWrap() {
    if (_selectedUsersIds.isEmpty) {
      return const Text('You have not selected any member yet.');
    } else {
      return Selector<UserProvider, List<User>>(
        selector: (_, userProvider) => userProvider.getUsersByIds(
          _selectedUsersIds.toList(),
        ),
        builder: (context, selectedUsers, _) => Wrap(
          spacing: 8.0,
          children: selectedUsers.map((selectedUser) {
            return Selector2<UserProvider, AuthProvider, String?>(selector: (_, userProvider, authProvider) {
              final emailCurrentUser = authProvider.currentUser?.email.toString();
              final currentUserId = userProvider.getUserByEmail(emailCurrentUser).id;
              return currentUserId;
            }, builder: (context, currentUserId, _) {
              return Chip(
                avatar: CircleAvatar(
                  backgroundImage: Image.network('${selectedUser.imageUrl}').image,
                ),
                label: Text(selectedUser.name),
                onDeleted: selectedUser.id == currentUserId
                    ? null
                    : () {
                        setState(() {
                          _selectedUsersIds.remove(selectedUser.id);
                        });
                      },
              );
            });
          }).toList(),
        ),
      );
    }
  }

  Widget _buildUsersPicker() {
    const Widget verticalSmallDivider = SizedBox(height: 8.0);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'Members',
          style: TextStyle(
            fontWeight: FontWeight.w600,
          ),
        ),
        verticalSmallDivider,
        _buildSelectedUsersWrap(),
        verticalSmallDivider,
        ElevatedButton(
          onPressed: () async {
            final selectedUser = await showDialog<User?>(
              context: context,
              builder: (context) => const Dialog(
                child: SelectUserDialog(),
              ),
            );

            if (selectedUser != null) {
              setState(() {
                _selectedUsersIds.add(selectedUser.id);
              });
            }
          },
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith((_) => BookFriendsColor.socialPink),
          ),
          child: const Text(
            'Add new member',
            style: TextStyle(
              color: Colors.white,
              fontSize: 14.0,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildEndDatePicker() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'End date:',
          style: TextStyle(
            fontWeight: FontWeight.w600,
          ),
        ),
        ElevatedButton(
          style: ButtonStyle(
            shape: MaterialStateProperty.resolveWith(
              (_) => RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(29),
              ),
            ),
            backgroundColor: MaterialStateProperty.resolveWith((_) => BookFriendsColor.socialPink),
          ),
          onPressed: () async {
            final selectedDate = await showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime.now(),
              lastDate: DateTime.now().add(
                const Duration(days: 10000),
              ),
              initialDatePickerMode: DatePickerMode.year,
            );

            setState(() {
              _selectedEndDate = selectedDate;
            });
          },
          child: Text(
            _selectedEndDate != null
                ? '${_selectedEndDate?.toIso8601String()}'
                : 'Insert an end date for the reading plan',
            style: const TextStyle(
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildCreateReadingPlanButton() {
    final isAllowed = _selectedEndDate != null &&
        _selectedUsersIds.isNotEmpty &&
        _titleController.text.isNotEmpty &&
        _selectedBookId != null;

    return Center(
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith((_) => BookFriendsColor.socialPink),
        ),
        onPressed: isAllowed
            ? () async {
                final readingPlanProvider = context.read<ReadingPlanProvider>();
                await readingPlanProvider.createReadingPlan(
                  title: _titleController.text,
                  usersIds: _selectedUsersIds.toList(),
                  bookId: '$_selectedBookId',
                  endDate: _selectedEndDate ?? DateTime.now(),
                );
                Navigator.of(context).pop();
              }
            : null,
        child: const Text(
          'Create reading plan',
          style: TextStyle(
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(backgroundColor: BookFriendsColor.darkBlack, title: _buildHeader(), centerTitle: true),
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: SafeArea(
          child: ListView(
            physics: const BouncingScrollPhysics(),
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(
              vertical: 32.0,
              horizontal: 32.0,
            ),
            children: [
              _buildTextField(controller: _titleController, label: 'Title'),
              _verticalSpace,
              _buildBookDropDown(),
              _verticalSpace,
              _buildUsersPicker(),
              _verticalSpace,
              _buildEndDatePicker(),
              _verticalSpace,
              _buildCreateReadingPlanButton(),
            ],
          ),
        ),
      ),
    );
  }
}
