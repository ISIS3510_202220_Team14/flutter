import 'package:bookfriends/core/models/reading_plan.dart';
import 'package:bookfriends/core/providers/discussion_provider.dart';
import 'package:bookfriends/ui/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../../core/event_report.dart';
import '../../core/models/book.dart';
import '../../core/models/discussion.dart';
import '../../core/providers/book_provider.dart';
import '../../core/providers/reading_plan_provider.dart';
import '../widgets/detail_discussion.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

const styleTitle = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

const styleSub = TextStyle(
  fontSize: 15,
  color: BookFriendsColor.socialPink,
);

const styleAdd = TextStyle(
  fontSize: 16,
  color: BookFriendsColor.socialBlue,
);

class DiscussionView extends StatefulWidget {
  final String readingPlanId;
  const DiscussionView({super.key, required this.readingPlanId});

  @override
  State<DiscussionView> createState() => _DiscussionViewState();
}

class _DiscussionViewState extends State<DiscussionView> {
  _DiscussionViewState();

  @override
  void initState() {
    super.initState();
  }

  final _formKeyDiscussion = GlobalKey<FormState>();
  TextEditingController inputDiscussion = TextEditingController();

  Widget _buildHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: const [
        Text(
          'Discussion Board',
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget discussionBox(Discussion discussion) {
    int numCom = discussion.commentsIds.length;
    String infoComments = '$numCom comments';
    if (numCom == 0) {
      infoComments = 'No comments';
    } else if (numCom == 1) {
      infoComments = '$numCom comment';
    }
    return Card(
      elevation: 8.0,
      margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: ListTile(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        title: Text(
          discussion.topic,
        ),
        subtitle: Row(
          children: <Widget>[
            Expanded(flex: 4, child: Text(infoComments, style: styleSub))
          ],
        ),
        trailing: const Icon(Icons.keyboard_arrow_right,
            color: BookFriendsColor.socialBlue, size: 30.0),
        onTap: () {
          final discussionProvider = context.read<DiscussionProvider>();
          discussionProvider.addView(discussion.id);
          sendDiscussionEvent(discussion.topic);
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DetailDiscussion(
                        discussionId: discussion.id,
                      )));
        },
      ),
    );
  }

  Widget _buildBookInfo(Book book) {
    return Column(
      children: [
        CachedNetworkImage(
          imageUrl: book.imageUrl,
          imageBuilder: (context, imageProvider) => Container(
            width: 150,
            height: 150.0,
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              image:
                  DecorationImage(image: imageProvider, fit: BoxFit.fitHeight),
            ),
          ),
          fit: BoxFit.fitHeight,
          placeholder: (context, url) => Image.asset(
              'assets/images/no_book.jpg',
              fit: BoxFit.fitHeight,
              height: 150.0),
          errorWidget: (context, url, error) => Image.asset(
              'assets/images/no_book.jpg',
              fit: BoxFit.fitHeight,
              height: 150.0),
        ),
        Text(
          '\nDiscussion: ${book.name}',
          style: styleTitle,
        ),
      ],
    );
  }

  Widget _newDiscussion() {
    return Card(
      color: Colors.transparent,
      elevation: 0.0,
      margin: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
      child: ListTile(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        title: Column(
          children: const [
            Text(
              'Add a new Discussion Topic !',
              style: styleAdd,
            ),
          ],
        ),
        subtitle: Form(
          key: _formKeyDiscussion,
          child: Column(
            children: <Widget>[
              TextFormField(
                inputFormatters: [
                  LengthLimitingTextInputFormatter(150),
                ],
                keyboardType: TextInputType.multiline,
                maxLines: null,
                controller: inputDiscussion,
                validator: (value) {
                  value = value?.trim();
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: const InputDecoration(
                  filled: true,
                  fillColor: Colors.transparent,
                  border: OutlineInputBorder(),
                  hintText: 'Enter a new discussion topic',
                ),
              )
            ],
          ),
        ),
        trailing: const Icon(Icons.add,
            color: BookFriendsColor.socialBlue, size: 30.0),
        onTap: () {
          _checkConnectionInput();
        },
      ),
    );
  }

  Widget makeBody(List<String> discussionsIds) {
    return Selector<DiscussionProvider, List<Discussion>>(
        selector: (_, discussionProvider) =>
            discussionProvider.getDiscussionsByIds(discussionsIds),
        builder: (context, discussions, _) => SafeArea(
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 8.0,
                        ),
                        child: discussionBox(
                          discussions[index],
                        ),
                      ),
                  itemCount: discussions.length),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: BookFriendsColor.darkBlack,
          title: _buildHeader(),
          centerTitle: true,
        ),
        body: Selector<ReadingPlanProvider, ReadingPlan>(
            selector: (_, readingPlanProvider) =>
                readingPlanProvider.getReadingPlanById(widget.readingPlanId),
            builder: (context, readingPlan, _) => Selector<BookProvider, Book>(
                selector: (_, bookProvider) =>
                    bookProvider.getBookById(readingPlan.bookId),
                builder: (context, book, _) => SafeArea(
                        child: ListView(
                            physics: const BouncingScrollPhysics(),
                            padding: const EdgeInsets.symmetric(
                              vertical: 0.0,
                            ),
                            children: [
                          _buildBookInfo(book),
                          _newDiscussion(),
                          makeBody(readingPlan.discussionsIds)
                        ])))));
  }

  _managePostDiscussion(String topic) {
    sendFeatureEvent('new_discussion');
    DiscussionProvider().addDiscussion(topic, widget.readingPlanId);
  }

  _checkConnectionInput() async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      if (_formKeyDiscussion.currentState!.validate()) {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
              content: Text('Posting new Discussion...'),
              duration: Duration(milliseconds: 500)),
        );
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        _managePostDiscussion(inputDiscussion.text)
            .then(
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                      content: Text('New discussion successfully added'),
                      duration: Duration(seconds: 2)),
                ),
                inputDiscussion.clear())
            .catchError(ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                  content: Text(
                      'There was an unexpected error, please try again later'),
                  duration: Duration(seconds: 5)),
            ));
      }
    } else {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('There is no Internet Connection. Try again Later'),
            duration: Duration(seconds: 2)),
      );
    }
  }
}
