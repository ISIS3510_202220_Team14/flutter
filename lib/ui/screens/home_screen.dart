import 'package:bookfriends/core/providers/post_provider.dart';
import 'package:bookfriends/ui/widgets/menu.dart';
import 'package:bookfriends/ui/widgets/post_box.dart';
import 'package:bookfriends/ui/widgets/reading_plan_box.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:bookfriends/core/providers/theme_provider.dart';
import 'package:bookfriends/theme/theme_constants.dart';
import '../../core/models/post.dart';
import '../../core/models/reading_plan.dart';
import '../../core/providers/auth_provider.dart';
import '../../core/providers/reading_plan_provider.dart';
import 'dart:core';
import '../../core/providers/user_provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController _textController = TextEditingController();
  List<String> _searchPostIds = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Widget _buildHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: const [
        Text(
          'ReadBooks',
          style: TextStyle(
            color: Colors.white,
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget searchBar() {
    return TextField(
      controller: _textController,
      onChanged: (search) async {
        final searchPostIdsResult =
            await context.read<PostProvider>().searchByWord(search);
        setState(() {
          _searchPostIds = searchPostIdsResult;
        });
      },
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.search),
        isDense: true,
        hintText: 'Search',
        border: OutlineInputBorder(),
      ),
    );
  }

  Widget _buildSearchBar() {
    return searchBar();
  }

  Widget _buildFeedReadingPlans() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(
            bottom: 8.0,
            left: 24.0,
            right: 24.0,
            top: 24.0,
          ),
          child: Text(
            'Your reading plans:',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        SizedBox(
          height: 160.0,
          child: _buildReadingPlanListView(),
        )
      ],
    );
  }

  Widget _buildReadingPlanListView() {
    return Selector3<AuthProvider, UserProvider, ReadingPlanProvider,
        List<ReadingPlan>>(
      selector: (_, authProvider, userProvider, readingPlanProvider) {
        final emailCurrentUser = authProvider.currentUser?.email.toString();
        final currentUserId = userProvider.getUserByEmail(emailCurrentUser).id;
        return readingPlanProvider.getReadingPlansByUserId(currentUserId);
      },
      builder: (context, readingPlans, _) => ListView.separated(
        itemCount: readingPlans.length,
        padding: const EdgeInsets.symmetric(
          horizontal: 24.0,
          vertical: 10.0,
        ),
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => AspectRatio(
          aspectRatio: 0.65,
          child: ReadingPlanBox(
            readingPlan: readingPlans[index],
          ),
        ),
        separatorBuilder: (context, index) => const VerticalDivider(
          color: Colors.transparent,
        ),
      ),
    );
  }

  Widget _buildDivider() {
    return const Divider(
      thickness: 1.5,
      color: Colors.grey,
      height: 32.0,
    );
  }

  Widget _buildPostsListView() {
    return Selector<PostProvider, List<Post>>(
      selector: (_, postProvider) {
        if (_textController.text.isEmpty) {
          return postProvider.posts;
        } else {
          return postProvider.getPostsByIds(_searchPostIds);
        }
      },
      builder: (context, posts, _) => ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 24.0,
          ),
          child: PostBox(
            post: posts[index],
          ),
        ),
        separatorBuilder: (context, index) => _buildDivider(),
        itemCount: posts.length,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ThemeChanger>(context);
    var darkmode = (provider.getTheme() == MyThemes.darkTheme);
    //_mostrarAlerta(context);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).primaryColor,
      drawer: Menu(scaffoldKey: _scaffoldKey),
      appBar: AppBar(
        backgroundColor: darkBlack,
        leading: IconButton(
          icon: const Icon(Icons.menu_outlined),
          onPressed: () {
            _scaffoldKey.currentState?.openDrawer();
          },
        ),
        title: _buildHeader(),
        centerTitle: true,
        actions: [
          IconButton(
            icon: darkmode
                ? const Icon(Icons.wb_sunny)
                : const Icon(Icons.dark_mode),
            onPressed: () {
              //_mostrarAlerta(context);
              if (darkmode) {
                provider.setTheme(MyThemes.lightTheme);
                setState(() {
                  darkmode = !darkmode;
                });
              } else {
                provider.setTheme(MyThemes.darkTheme);
                setState(() {
                  darkmode = !darkmode;
                });
              }
            },
          ),
          IconButton(
            icon: const Icon(Icons.logout_outlined),
            onPressed: () {
              final authProvider = context.read<AuthProvider>();
              authProvider.logOut(context);
            },
          ),
        ],
      ),
      body: SafeArea(
        child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            _buildFeedReadingPlans(),
            _buildDivider(),
            _buildSearchBar(),
            const SizedBox(height: 24.0),
            _buildPostsListView()
          ],
        ),
      ),
    );
  }
}
