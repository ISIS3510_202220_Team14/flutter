import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:bookfriends/ui/constants/colors.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:bookfriends/core/models/place.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import '../../core/event_report.dart';
import '../widgets/place_layout.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

const String key = 'AIzaSyBlesqmMWb-jRI_Ue9hm00BPIgoW1kDwWE';
const styleDescription = TextStyle(fontSize: 18);

class LocationPage extends StatefulWidget {
  const LocationPage({Key? key}) : super(key: key);

  @override
  State<LocationPage> createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  Position? _currentPosition;
  late Future<PlaceList> _futurePlaces;
  bool searching = false;
  double _currentSliderValue = 5;
  late Stream<FileResponse> fileStream;

  void _downloadFile(String url) {
    setState(() {
      fileStream = DefaultCacheManager().getFileStream(url, withProgress: true);
    });
  }

  Future<PlaceList> fetchPlacesDefault() async {
    Future<PlaceList> places = [] as Future<PlaceList>;
    return places;
  }

  Future<PlaceList> fetchPlacesNew(Position position) async {
    double radio = 10000;
    double lat = position.latitude;
    double long = position.longitude;
    //double lat = 4.734532229957328;
    //double long = -74.03408847423408;
    var urlLink =
        'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radio&type=book_store&key=$key';

    try {
      var first = await DefaultCacheManager().getFileFromCache('places');
      if (first == null) {
        await DefaultCacheManager().downloadFile(urlLink, key: 'places');
      }
      first = await DefaultCacheManager().getFileFromCache('places');

      var path = first!.file.path;
      final file = File(path);
      final contents = await file.readAsString();
      final jsonData = json.decode(contents)['results'];
      return PlaceList.fromJson(jsonData, lat, long);
    } catch (e) {
      throw Exception('Failed to load places');
    }
  }

  @override
  void initState() {
    super.initState();
    _futurePlaces = fetchPlacesDefault();
    _getCurrentPosition();
  }

  Widget _setDistance() {
    return Slider(
      value: _currentSliderValue,
      min: 1,
      max: 10,
      divisions: 10,
      activeColor: BookFriendsColor.socialPink,
      label: _currentSliderValue.round().toString(),
      onChanged: (double value) {
        setState(() {
          _currentSliderValue = value;
        });
      },
    );
  }

  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          duration: const Duration(seconds: 10),
          content: const Text(
              'Location services are disabled. Please enable the services'),
          action: SnackBarAction(
            label: 'Go to location settings',
            onPressed: () {
              Geolocator.openLocationSettings();
              _getCurrentPosition();
            },
          )));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: const Duration(seconds: 10),
            content: const Text('Location permissions are denied'),
            action: SnackBarAction(
              label: 'Go to location settings',
              onPressed: () {
                Geolocator.openLocationSettings();
                _getCurrentPosition();
              },
            )));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          duration: const Duration(seconds: 10),
          content: const Text(
              'Location permissions are permanently denied, we cannot request permissions.'),
          action: SnackBarAction(
            label: 'Go to location settings',
            onPressed: () {
              Geolocator.openLocationSettings();
              _getCurrentPosition();
            },
          )));
      return false;
    }
    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();
    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() => {
            _currentPosition = position,
          });
    }).catchError((e) {
      debugPrint(e);
    });
  }

  _refreshPlaces() async {
    sendFeatureEvent('find_bookstores');
    searching = true;
    if (_currentPosition != null) {
      setState(() => {_futurePlaces = fetchPlacesNew(_currentPosition!)});
    }
  }

  Widget _buildInfo() {
    return _currentPosition == null
        ? const Text('')
        : Column(
            children: [
              Row(
                children: [
                  Expanded(
                      child: Column(
                    children: [
                      const Text(
                        '\nDefine distance range (Km):',
                        style: styleDescription,
                      ),
                      _setDistance()
                    ],
                  )),
                  TextButton(
                      onPressed: () =>
                          !searching ? _checkConnectionRefresh() : null,
                      child: Container(
                        padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                        color: BookFriendsColor.socialBlue,
                        child: const Text(
                          'Search',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ))
                ],
              ),
              Center(
                child: FutureBuilder<PlaceList>(
                  future: _futurePlaces,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData && searching) {
                      return Center(
                          child: Column(
                        children: const [
                          Text(
                            'Searching bookstores...\n',
                            style: styleDescription,
                          ),
                          CircularProgressIndicator()
                        ],
                      ));
                    } else if (snapshot.hasData) {
                      return PlaceLayout(
                          places: snapshot.data!,
                          distance: _currentSliderValue);
                    } else if (snapshot.hasError) {
                      return Column(
                        children: const [],
                      );
                    }
                    return const Text(
                      'Waiting for location services to be activated',
                      style: styleDescription,
                    );
                  },
                ),
              )
            ],
          );
  }

  Widget _buildHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: const [
        Text(
          'Find Bookstores Near Me!',
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: BookFriendsColor.darkBlack,
          title: _buildHeader(),
          centerTitle: true,
        ),
        body: SafeArea(
            child: ListView(
                physics: const BouncingScrollPhysics(),
                padding: const EdgeInsets.symmetric(
                  vertical: 0.0,
                ),
                children: [
              Center(
                child: Column(children: [
                  _buildInfo(),
                  _getLocation(),
                ]),
              )
            ])));
  }

  Widget _getLocation() {
    return SizedBox(
        height: 100,
        child: _currentPosition == null
            ? Center(
                child: Column(children: const [
                Text(
                  'Getting location...\n',
                  style: styleDescription,
                ),
                CircularProgressIndicator()
              ]))
            : const Text(''));
  }

  _checkConnectionRefresh() async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      _refreshPlaces();
    } else {
      _refreshPlaces();
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      return ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text(
            'There is no Internet connection.\nChecking availability in cache'),
      ));
    }
  }
}
