import 'package:bookfriends/core/models/note.dart';
import 'package:bookfriends/core/providers/auth_provider.dart';
import 'package:bookfriends/core/providers/book_provider.dart';
import 'package:bookfriends/core/providers/note_provider.dart';
import 'package:bookfriends/core/providers/user_provider.dart';
import 'package:bookfriends/ui/screens/create_note_screen.dart';
import 'package:bookfriends/ui/utils/parse_utils.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';
import '../constants/colors.dart';

class NotesPage extends StatefulWidget {
  const NotesPage({super.key});

  @override
  State<NotesPage> createState() => NotesPageState();
}

class NotesPageState extends State<NotesPage> {
  Widget _buildHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: const [
        Text(
          'My Notes',
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: BookFriendsColor.darkBlack,
          title: _buildHeader(),
          centerTitle: true,
        ),
        body: Selector3<AuthProvider, UserProvider, NoteProvider, List<Note>>(
          selector: (_, authProvider, userProvider, noteProvider) {
            final emailCurrentUser = authProvider.currentUser?.email.toString();
            final currentUserId =
                userProvider.getUserByEmail(emailCurrentUser).id;
            return noteProvider.getNotesByUser(currentUserId);
          },
          builder: (context, notes, _) => SafeArea(
              child: ListView(
                  physics: const BouncingScrollPhysics(),
                  padding: const EdgeInsets.symmetric(
                    vertical: 0.0,
                  ),
                  children: [
                _newNote(),
                _buildNotes(notes),
              ])),
        ));
  }

  Widget noteBox(Note note) {
    return Selector<BookProvider, String>(
        selector: (_, bookProvider) =>
            bookProvider.getBookById(note.bookId).name,
        builder: (context, bookName, _) => Card(
              elevation: 8.0,
              margin:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: ListTile(
                contentPadding: const EdgeInsets.symmetric(
                    horizontal: 20.0, vertical: 10.0),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 0,
                      child: Text(
                        note.title,
                        style: const TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Flexible(
                      flex: 0,
                      child: Text(
                        bookName,
                        style: const TextStyle(fontSize: 16),
                      ),
                    )
                  ],
                ),
                subtitle: Column(
                  children: [
                    Row(
                      children: <Widget>[
                        Flexible(
                          child: Text(
                            ParseUtils.toDaysAgo(note.timestamp),
                            style: const TextStyle(
                                fontSize: 12, color: Colors.teal),
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Flexible(
                          child: Text(
                            /*
                            note.description.length <= 90
                                ? note.description
                                : '${note.description.substring(0, 90)}...',*/
                            note.description,
                            style: const TextStyle(fontSize: 15),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ));
  }

  Widget _buildNotes(List<Note> notes) {
    return SafeArea(
      child: ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8.0,
                ),
                child: noteBox(
                  notes[index],
                ),
              ),
          itemCount: notes.length),
    );
  }

  Widget _newNote() {
    return Column(
      children: [
        ElevatedButton.icon(
          onPressed: () {
            _checkConnectionNote();
          },
          label: const Text('Add note', style: TextStyle(fontSize: 20)),
          icon: const Icon(
            Icons.add,
            size: 20.0,
          ),
        )
      ],
    );
  }

  _checkConnectionNote() async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => const CreateNoteScreen()));
    } else {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('There is no Internet Connection. Try again Later'),
            duration: Duration(seconds: 2)),
      );
    }
  }
}
