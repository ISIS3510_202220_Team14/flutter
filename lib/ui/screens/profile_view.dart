import 'package:bookfriends/core/providers/auth_provider.dart';
import 'package:bookfriends/ui/widgets/readingplan_profile_box.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bookfriends/ui/constants/colors.dart';
import 'package:bookfriends/ui/widgets/friend_profile_box.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import '../../core/models/reading_plan.dart';
import '../../core/models/user.dart';
import '../../core/providers/reading_plan_provider.dart';
import '../../core/providers/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cached_network_image/cached_network_image.dart';

const styleUsername = TextStyle(
  fontSize: 14,
  color: BookFriendsColor.socialPink,
);

class ProfileView extends StatefulWidget {
  final User user;
  const ProfileView({required this.user, super.key});

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  Widget _buildHeader(String currentUserId) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          currentUserId != widget.user.id ? 'Profile' : 'My Profile',
          style: const TextStyle(
            color: Colors.white,
            fontSize: 22.0,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget _buildBackPhoto() {
    return SizedBox(
      width: double.maxFinite,
      height: 170,
      child: Image.asset('assets/images/background_profile.jpg',
          fit: BoxFit.fitWidth),
    );
  }

  Widget _buildSocialMediaPanel(String firstName, String lastName) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: const FaIcon(
            FontAwesomeIcons.facebook,
            size: 20,
          ),
          tooltip: 'Facebook',
          onPressed: () {
            _checkConnectionSocialMedia('facebook', firstName, lastName);
          },
        ),
        IconButton(
          icon: const FaIcon(
            FontAwesomeIcons.instagram,
            size: 20,
          ),
          tooltip: 'Instagram',
          onPressed: () {
            _checkConnectionSocialMedia('instagram', firstName, lastName);
          },
        ),
        IconButton(
          icon: const FaIcon(
            FontAwesomeIcons.twitter,
            size: 20,
          ),
          tooltip: 'Twitter',
          onPressed: () {
            _checkConnectionSocialMedia('twitter', firstName, lastName);
          },
        ),
      ],
    );
  }

  Widget _buildDataProfile(User user) {
    return Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
      Expanded(
          child: Transform.translate(
              offset: const Offset(0, -50),
              child: CachedNetworkImage(
                imageUrl: '${user.imageUrl}',
                imageBuilder: (context, imageProvider) => Container(
                  width: 150.0,
                  height: 150.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
                fit: BoxFit.fill,
                placeholder: (context, url) => const CircleAvatar(
                  radius: 70,
                  backgroundImage:
                      AssetImage('assets/images/no_image_user.png'),
                ),
                errorWidget: (context, url, error) => const CircleAvatar(
                  radius: 70,
                  backgroundImage:
                      AssetImage('assets/images/no_image_user.png'),
                ),
              ))),
      Expanded(
          child: Padding(
              padding: const EdgeInsets.only(bottom: 40.0),
              child: Column(
                children: [
                  const Text(' '),
                  Text(user.firstName,
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.bold)),
                  Text(user.lastName, style: styleUsername),
                  _buildSocialMediaPanel(user.firstName, user.lastName),
                  const Text(' '),
                ],
              )))
    ]);
  }

  Widget _buildDivider() {
    return const Divider(
      thickness: 0.0,
      color: Colors.grey,
      height: 0.0,
    );
  }

  Widget _buildFriends(List<User> friends) {
    return SafeArea(
        child: ListView(
            physics: const BouncingScrollPhysics(),
            padding: const EdgeInsets.symmetric(
              vertical: 0.0,
            ),
            children: [
          friends.isEmpty
              ? Padding(
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    '${widget.user.name} does not have any friends yet.',
                    style: const TextStyle(fontSize: 16),
                  ),
                )
              : ListView.separated(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) => Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8.0,
                    ),
                    child: FriendProfileBox(
                      friend: friends[index],
                    ),
                  ),
                  separatorBuilder: (context, index) => _buildDivider(),
                  itemCount: friends.length,
                ),
        ]));
  }

  Widget _buildReadingPlans(List<ReadingPlan> readingPlans, String userId) {
    List<ReadingPlan> readingPlanFilter = readingPlans
        .where(
          (element) => element.usersIds.contains(userId),
        )
        .toList();
    return readingPlanFilter.isEmpty
        ? Padding(
            padding: const EdgeInsets.all(20),
            child: Text(
              '${widget.user.name} does not have any reading plans yet.',
              style: const TextStyle(fontSize: 16),
            ),
          )
        : SafeArea(
            child: ListView(
                physics: const BouncingScrollPhysics(),
                padding: const EdgeInsets.symmetric(
                  vertical: 0.0,
                ),
                children: [
                ListView.separated(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) => Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8.0,
                    ),
                    child: ReadingPlanProfileBox(
                      readingPlan: readingPlanFilter[index],
                    ),
                  ),
                  separatorBuilder: (context, index) => _buildDivider(),
                  itemCount: readingPlanFilter.length,
                )
              ]));
  }

  Widget _buildStatistics(List<ReadingPlan> readingPlans, String userId) {
    List<ReadingPlan> readingPlanFilter = readingPlans
        .where(
          (element) => element.usersIds.contains(userId),
        )
        .toList();
    return Padding(
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Number of Reading Plans: ${readingPlanFilter.length}',
              style: const TextStyle(
                fontSize: 16,
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Selector2<AuthProvider, UserProvider, User>(
        selector: (_, authProvider, userProvider) {
          final emailCurrentUser = authProvider.currentUser?.email.toString();
          final currentUser = userProvider.getUserByEmail(emailCurrentUser);
          return currentUser;
        },
        builder: (context, currentUser, _) => Scaffold(
            appBar: AppBar(
              backgroundColor: BookFriendsColor.darkBlack,
              title: _buildHeader(currentUser.id),
              actions: [
                currentUser.id != widget.user.id
                    ? IconButton(
                        onPressed: () => {addFriend(currentUser, widget.user)},
                        icon: const Icon(Icons.person_add))
                    : const Text('')
              ],
              centerTitle: true,
            ),
            body: Selector<UserProvider, User>(
              selector: (_, userProvider) =>
                  userProvider.getUserById(widget.user.id),
              builder: (context, profileUser, _) =>
                  Selector<UserProvider, List<User>>(
                selector: (_, userProvider) =>
                    userProvider.getUsersByIds(profileUser.friendsIds),
                builder: (context, friends, _) =>
                    Selector<ReadingPlanProvider, List<ReadingPlan>>(
                  selector: (_, readingPlanProvider) =>
                      readingPlanProvider.readingPlans,
                  builder: (context, readingPlans, _) => DefaultTabController(
                    length: 3,
                    initialIndex: 0,
                    child: Column(
                      children: [
                        _buildBackPhoto(),
                        _buildDataProfile(widget.user),
                        Transform.translate(
                          offset: const Offset(0, -50),
                          child: Column(
                            children: [
                              const TabBar(
                                labelColor: Colors.teal,
                                tabs: [
                                  Tab(
                                    child: Text('Friends',
                                        style: TextStyle(
                                          fontSize: 16,
                                        )),
                                  ),
                                  Tab(
                                    child: Text('Reading Plans',
                                        style: TextStyle(
                                          fontSize: 16,
                                        )),
                                  ),
                                  Tab(
                                    child: Text('Statistics',
                                        style: TextStyle(
                                          fontSize: 16,
                                        )),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: double.maxFinite,
                                height: 300.0,
                                child: TabBarView(
                                  children: [
                                    _buildFriends(friends),
                                    _buildReadingPlans(
                                        readingPlans, profileUser.id),
                                    _buildStatistics(
                                        readingPlans, profileUser.id),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )));
  }

  _checkConnectionSocialMedia(
      String socialMedia, String firstName, String lastName) async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      _launchURLApp(socialMedia, firstName, lastName);
    } else {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
              'There is no Internet connection.\nWould you like yo continue to $socialMedia?'),
          action: SnackBarAction(
            label: 'Continue',
            onPressed: () {
              _launchURLApp(socialMedia, firstName, lastName);
            },
          )));
    }
  }

  _launchURLApp(String socialMedia, String firstName, String lastName) async {
    String fbProtocolUrl = 'instagram://page/';
    String fallbackUrl = 'https://www.instagram.com/$firstName$lastName/';
    if (socialMedia == 'facebook') {
      fbProtocolUrl = 'facebook://page/';
      fallbackUrl = 'https://es-la.facebook.com/public/$firstName%20$lastName';
    } else if (socialMedia == 'twitter') {
      fbProtocolUrl = 'twitter://page/';
      fallbackUrl =
          'https://twitter.com/search?q=$firstName%20$lastName&src=typed_query';
    }
    try {
      Uri fbBundleUri = Uri.parse(fbProtocolUrl);
      var canLaunchNatively = await canLaunchUrl(fbBundleUri);

      if (canLaunchNatively) {
        launchUrl(fbBundleUri);
      } else {
        await launchUrl(Uri.parse(fallbackUrl),
            mode: LaunchMode.externalApplication);
      }
    } catch (e) {
      // Handle this as you prefer
    }
  }

  addFriend(User currentUser, User newFriend) async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      try {
        final userProvider = context.read<UserProvider>();
        userProvider.addFriend(currentUser, newFriend.id);
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              content:
                  Text('${widget.user.name} has been added to your friends.')),
        );
      } catch (error) {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('A problem has occurred. Try again')),
        );
      }
    } else {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('There is no Internet Connection. Try again Later')),
      );
    }
  }
}
