import 'dart:math';

import 'package:bookfriends/core/models/reading_plan.dart';
import 'package:bookfriends/core/models/user.dart';
import 'package:bookfriends/core/providers/book_provider.dart';
import 'package:bookfriends/ui/constants/colors.dart';
import 'package:bookfriends/ui/screens/discussion_view.dart';
import 'package:bookfriends/ui/utils/parse_utils.dart';
//import 'package:bookfriends/ui/widgets/alert_connectivity.dart';
import 'package:bookfriends/ui/widgets/profile_nav_wrapper.dart';
import 'package:bookfriends/ui/widgets/reading_plan_box.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../core/event_report.dart';
import '../../core/models/book.dart';
import '../../core/providers/user_provider.dart';

class ReadingPlanScreen extends StatelessWidget {
  final ReadingPlan readingPlan;

  const ReadingPlanScreen({required this.readingPlan, super.key});

  Widget _buildHeader() {
    return Center(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            'Reading Plan',
            style: TextStyle(
              color: Colors.white,
              fontSize: 22.0,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(
            width: 60.0,
          ),
          const Icon(
            Icons.remove_red_eye,
            size: 18.0,
          ),
          const SizedBox(
            width: 6.0,
          ),
          Text(
            readingPlan.views.toString(),
            style: const TextStyle(
              fontSize: 14.0,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBook() {
    return Selector<BookProvider, Book>(
      selector: (_, bookProvider) => bookProvider.getBookById(readingPlan.bookId),
      builder: (context, book, _) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Book:',
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(
            height: 8.0,
          ),
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  book.name,
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 8.0,
                ),
                Text(
                  book.author,
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(
                  height: 8.0,
                ),
                SizedBox(
                  width: 160.0,
                  child: AspectRatio(
                    aspectRatio: 0.65,
                    child: ReadingPlanBox(
                      readingPlan: readingPlan,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildParticipants() {
    return Selector<UserProvider, List<User>>(
      selector: (_, userProvider) => userProvider.getUsersByIds(readingPlan.usersIds),
      builder: (context, participants, _) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Members:',
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(
            height: 8.0,
          ),
          Wrap(
            runSpacing: 8.0,
            spacing: 8.0,
            children: participants
                .map(
                  (participant) => ProfileNavWrapper(
                      user: participant,
                      child: CachedNetworkImage(
                        imageUrl: '${participant.imageUrl}',
                        imageBuilder: (context, imageProvider) => Container(
                          width: 60.0,
                          height: 60.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                          ),
                        ),
                        fit: BoxFit.fill,
                        placeholder: (context, url) => const CircleAvatar(
                          radius: 28,
                          backgroundImage: AssetImage('assets/images/no_image_user.png'),
                        ),
                        errorWidget: (context, url, error) => const CircleAvatar(
                          radius: 28,
                          backgroundImage: AssetImage('assets/images/no_image_user.png'),
                        ),
                      )),
                )
                .toList(),
          ),
        ],
      ),
    );
  }

  Widget _buildRow(String subtitle, String text) {
    return Row(
      children: [
        Text(
          '$subtitle:',
          style: const TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        const SizedBox(
          width: 8.0,
        ),
        Text(
          text,
          style: const TextStyle(
            fontSize: 18.0,
          ),
        ),
      ],
    );
  }

  Widget _buildDateRow(String subtitle, DateTime date) {
    return _buildRow(
      subtitle,
      ParseUtils.toDaysAgo(date),
    );
  }

  Widget _buildDates() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildDateRow('Start date', readingPlan.startDate),
        if (readingPlan.endDate != null)
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: _buildDateRow('End date', readingPlan.endDate!),
          ),
      ],
    );
  }

  Widget _buildProgressRow(User user, double percentage) {
    return Row(
      children: [
        Expanded(
          child: Text(
            user.firstName,
            style: const TextStyle(
              fontSize: 18.0,
            ),
          ),
        ),
        const SizedBox(
          width: 12.0,
        ),
        SizedBox(
          width: 160.0,
          child: LinearProgressIndicator(value: percentage),
        ),
      ],
    );
  }

  Widget _buildProgress() {
    return Selector<UserProvider, List<User>>(
      selector: (_, userProvider) => userProvider.getUsersByIds(readingPlan.usersIds),
      builder: (context, participants, _) => Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const Text(
          'Progress:',
          style: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        const SizedBox(
          height: 8.0,
        ),
        ...participants
            .map(
              (participant) => _buildProgressRow(
                participant,
                Random().nextDouble(),
              ),
            )
            .toList(),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: BookFriendsColor.darkBlack,
        centerTitle: true,
        title: _buildHeader(),
      ),
      body: SafeArea(
        child: ListView(
          physics: const BouncingScrollPhysics(),
          padding: const EdgeInsets.symmetric(
            vertical: 28.0,
          ),
          children: [
            //const AlertConnectivity(),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24.0,
              ),
              child: _buildRow('Title', readingPlan.title),
            ),
            const SizedBox(
              height: 8.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24.0,
              ),
              child: _buildParticipants(),
            ),
            const SizedBox(
              height: 12.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24.0,
              ),
              child: _buildBook(),
            ),
            const SizedBox(
              height: 18.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24.0,
              ),
              child: _buildDates(),
            ),
            const SizedBox(
              height: 12.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24.0,
              ),
              child: _buildProgress(),
            ),
            const SizedBox(
              height: 12.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24.0,
              ),
              child: ElevatedButton(
                onPressed: () {
                  sendFeatureEvent('see_boardPosts');
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return DiscussionView(readingPlanId: readingPlan.id);
                      },
                    ),
                  );
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith((_) => BookFriendsColor.socialPink),
                ),
                child: const Text(
                  'Board Posts',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
