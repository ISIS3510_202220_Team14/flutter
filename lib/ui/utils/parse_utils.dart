class ParseUtils {
  static toDaysAgo(DateTime date) {
    final int days = DateTime.now().difference(date).inDays;

    if (days < 1) {
      return 'Today';
    }

    return '$days day(s) ago';
  }
}
