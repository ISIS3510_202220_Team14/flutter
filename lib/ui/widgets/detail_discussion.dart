import 'package:bookfriends/core/models/comment.dart';
import 'package:bookfriends/core/models/discussion.dart';
import 'package:bookfriends/core/models/user.dart';
import 'package:bookfriends/core/providers/comment_provider.dart';
import 'package:bookfriends/core/providers/discussion_provider.dart';
import 'package:bookfriends/core/providers/user_provider.dart';
import 'package:bookfriends/ui/widgets/profile_nav_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:bookfriends/ui/constants/colors.dart';
import 'package:flutter/services.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../../core/event_report.dart';
import '../../core/providers/auth_provider.dart';

class DetailDiscussion extends StatefulWidget {
  final String discussionId;
  const DetailDiscussion({super.key, required this.discussionId});
  @override
  State<DetailDiscussion> createState() => _DetailDiscussionState();
}

class _DetailDiscussionState extends State<DetailDiscussion> {
  _DetailDiscussionState();

  @override
  void initState() {
    super.initState();
  }

  final _formKeyComment = GlobalKey<FormState>();
  TextEditingController inputComment = TextEditingController();

  Widget _buildHeader() {
    final discussionProvider = context.read<DiscussionProvider>();
    final discussion =
        discussionProvider.getDiscussionById(widget.discussionId);
    return Center(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            'Discussion Comments',
            style: TextStyle(
              color: Colors.white,
              fontSize: 22.0,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(
            width: 10.0,
          ),
          const Icon(
            Icons.remove_red_eye,
            size: 18.0,
          ),
          const SizedBox(
            width: 6.0,
          ),
          Text(
            discussion.views.toString(),
            style: const TextStyle(
              fontSize: 14.0,
            ),
          ),
        ],
      ),
    );
  }

  Widget commentBox(Comment comment) {
    return Selector<UserProvider, User>(
        selector: (_, userProvider) => userProvider.getUserById(comment.userId),
        builder: (context, user, _) => Card(
              elevation: 8.0,
              margin:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                decoration: const BoxDecoration(color: Colors.transparent),
                child: ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10.0),
                  leading: const Icon(Icons.keyboard_arrow_left,
                      color: BookFriendsColor.socialBlue, size: 30.0),
                  title: Row(
                    children: [
                      ProfileNavWrapper(
                        user: user,
                        child: CachedNetworkImage(
                          imageUrl: '${user.imageUrl}',
                          imageBuilder: (context, imageProvider) => Container(
                            width: 40.0,
                            height: 40.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ),
                          fit: BoxFit.fill,
                          placeholder: (context, url) => const CircleAvatar(
                            radius: 20,
                            backgroundImage:
                                AssetImage('assets/images/no_image_user.png'),
                          ),
                          errorWidget: (context, url, error) =>
                              const CircleAvatar(
                            radius: 20,
                            backgroundImage:
                                AssetImage('assets/images/no_image_user.png'),
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                      Text(
                        user.firstName,
                      ),
                    ],
                  ),
                  subtitle: Row(
                    children: <Widget>[
                      Expanded(flex: 4, child: Text(comment.text))
                    ],
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ));
  }

  Widget _newComment(BuildContext context) {
    return Card(
      color: Colors.transparent,
      elevation: 0.0,
      margin: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
      child: ListTile(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        title: Column(
          children: const [
            Text(
              'Add a new Comment !',
              style: TextStyle(
                fontSize: 16,
                color: BookFriendsColor.socialBlue,
              ),
            ),
          ],
        ),
        subtitle: Form(
          key: _formKeyComment,
          child: Column(
            children: <Widget>[
              TextFormField(
                inputFormatters: [
                  LengthLimitingTextInputFormatter(150),
                ],
                keyboardType: TextInputType.multiline,
                maxLines: null,
                controller: inputComment,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: const InputDecoration(
                  filled: true,
                  fillColor: Colors.transparent,
                  border: OutlineInputBorder(),
                  hintText: 'Enter a new comment',
                ),
              )
            ],
          ),
        ),
        trailing: const Icon(Icons.add,
            color: BookFriendsColor.socialBlue, size: 30.0),
        onTap: () {
          _checkConnectionInput();
        },
      ),
    );
  }

  Widget _buildComments(List<Comment> comments, BuildContext context) {
    return SafeArea(
      child: ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 8.0,
          ),
          child: commentBox(comments[index]),
        ),
        itemCount: comments.length,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: BookFriendsColor.darkBlack,
          title: _buildHeader(),
          centerTitle: true,
        ),
        body: Selector<DiscussionProvider, Discussion>(
            selector: (_, discussionProvider) =>
                discussionProvider.getDiscussionById(widget.discussionId),
            builder: (context, discussion, _) =>
                Selector<CommentProvider, List<Comment>>(
                    selector: (_, commentProvider) => commentProvider
                        .getCommentsByIds(discussion.commentsIds),
                    builder: (context, comments, _) => SafeArea(
                            child: ListView(
                                physics: const BouncingScrollPhysics(),
                                padding: const EdgeInsets.symmetric(
                                  vertical: 0.0,
                                ),
                                children: [
                              Center(
                                child: Text(
                                  '\n${discussion.topic}',
                                  style: const TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              _newComment(context),
                              _buildComments(comments, context)
                            ])))));
  }

  _checkConnectionInput() async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      if (_formKeyComment.currentState!.validate()) {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
              content: Text('Posting new Comment...'),
              duration: Duration(milliseconds: 500)),
        );
        final authProvider = context.read<AuthProvider>();
        final userProvider = context.read<UserProvider>();
        final emailCurrentUser = authProvider.currentUser?.email.toString();
        final currentUserId = userProvider.getUserByEmail(emailCurrentUser).id;
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        _managePostComment(inputComment.text, currentUserId)
            .then(
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                      content: Text('New comment successfully added'),
                      duration: Duration(seconds: 2)),
                ),
                inputComment.clear())
            .catchError(ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                  content: Text(
                      'There was an unexpected error, please try again later'),
                  duration: Duration(seconds: 5)),
            ));
      }
    } else {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('There is no Internet Connection. Try again Later'),
        ),
      );
    }
  }

  _managePostComment(String text, String currentUserId) {
    sendFeatureEvent('new_comment');
    CommentProvider().addComment(text, widget.discussionId, currentUserId);
  }
}
