import 'package:bookfriends/ui/widgets/profile_nav_wrapper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:bookfriends/core/models/user.dart';

class FriendProfileBox extends StatelessWidget {
  final User friend;

  const FriendProfileBox({required this.friend, super.key});

  @override
  Widget build(BuildContext context) {
    return Transform.translate(
        offset: const Offset(0, 0),
        child: Card(
          elevation: 0,
          color: Colors.transparent,
          child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 5, 5, 5),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ProfileNavWrapper(
                          user: friend,
                          child: CachedNetworkImage(
                            imageUrl: '${friend.imageUrl}',
                            imageBuilder: (context, imageProvider) => Container(
                              width: 60.0,
                              height: 60.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.cover),
                              ),
                            ),
                            fit: BoxFit.fill,
                            placeholder: (context, url) => const CircleAvatar(
                              radius: 30,
                              backgroundImage:
                                  AssetImage('assets/images/no_image_user.png'),
                            ),
                            errorWidget: (context, url, error) =>
                                const CircleAvatar(
                              radius: 30,
                              backgroundImage:
                                  AssetImage('assets/images/no_image_user.png'),
                            ),
                          )),
                      Expanded(
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(20, 5, 5, 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    friend.firstName,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  Text(
                                    friend.lastName,
                                    style: const TextStyle(fontSize: 14),
                                  )
                                ],
                              )))
                    ],
                  ))),
        ));
  }
}
