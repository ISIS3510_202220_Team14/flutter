import 'package:bookfriends/core/models/user.dart';
import 'package:bookfriends/core/event_report.dart';
import 'package:bookfriends/ui/screens/books_screen.dart';
import 'package:bookfriends/ui/screens/create_book_screen.dart';
import 'package:bookfriends/ui/screens/create_post_screen.dart';
import 'package:bookfriends/ui/screens/create_reading_plan_screen.dart';
import 'package:bookfriends/ui/screens/location_page.dart';
import 'package:bookfriends/ui/screens/notes_page.dart';
import 'package:bookfriends/ui/screens/profile_view.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';

import '../../core/providers/auth_provider.dart';
import '../../core/providers/user_provider.dart';

class Menu extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  const Menu({required this.scaffoldKey, super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Theme.of(context).primaryColor,
      child: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              ExpansionTile(
                title: const Text('Posts'),
                leading: const Icon(Icons.post_add_outlined), //add icon
                childrenPadding:
                    const EdgeInsets.only(left: 60), //children padding
                children: [
                  ListTile(
                    title: const Text('Create a new Post'),
                    onTap: () {
                      _checkConnection(context, 'new_post');
                    },
                  ),
                  ListTile(
                    title: const Text('Delete a Post'),
                    onTap: () {
                      //action on press
                    },
                  ),
                  //more child menu
                ],
              ),
              ExpansionTile(
                title: const Text('Books'),
                leading: const Icon(Icons.menu_book_rounded), //add icon
                childrenPadding:
                    const EdgeInsets.only(left: 60), //children padding
                children: [
                  ListTile(
                    title: const Text('Create a new Book'),
                    onTap: () {
                      _checkConnection(context, 'new_book');
                    },
                  ),
                  ListTile(
                    title: const Text('All Books'),
                    onTap: () {
                      _checkConnection(context, 'books');
                    },
                  ),
                  //more child menu
                ],
              ),
              ExpansionTile(
                title: const Text('Reading Plans'),
                leading: const Icon(Icons.book_rounded), //add icon
                childrenPadding:
                    const EdgeInsets.only(left: 60), //children padding
                children: [
                  ListTile(
                    title: const Text('Create a new Reading Plan'),
                    onTap: () {
                      _checkConnection(context, 'new_readingPlan');
                    },
                  ),

                  ListTile(
                    title: const Text('Delete a Reading Plan'),
                    onTap: () {
                      //action on press
                    },
                  ),

                  //more child menu
                ],
              ),
              ExpansionTile(
                title: const Text('My Account'),
                leading: const Icon(Icons.account_box), //add icon
                childrenPadding:
                    const EdgeInsets.only(left: 60), //children padding
                children: [
                  ListTile(
                    title: const Text('My Profile'),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return Selector2<AuthProvider, UserProvider, User>(
                                selector: (_, authProvider, userProvider) {
                                  final emailCurrentUser = authProvider
                                      .currentUser?.email
                                      .toString();
                                  final currentUser = userProvider
                                      .getUserByEmail(emailCurrentUser);
                                  return currentUser;
                                },
                                builder: ((context, currentUser, child) =>
                                    ProfileView(
                                      user: currentUser,
                                    )));
                          },
                        ),
                      );
                    },
                  ),

                  ListTile(
                    title: const Text('Notes'),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return const NotesPage();
                          },
                        ),
                      );
                    },
                  ),

                  //more child menu
                ],
              ),
              ExpansionTile(
                title: const Text('Friends'),
                leading: const Icon(Icons.person), //add icon
                childrenPadding:
                    const EdgeInsets.only(left: 60), //children padding
                children: [
                  ListTile(
                    title: const Text('Add a friend'),
                    onTap: () {
                      //action on press
                    },
                  ),

                  ListTile(
                    title: const Text('Remove a friend'),
                    onTap: () {
                      //action on press
                    },
                  ),

                  //more child menu
                ],
              ),
              ListTile(
                leading: const Icon(Icons.location_pin),
                title: const Text('Find Bookstores'),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return const LocationPage();
                      },
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  void _checkConnection(BuildContext context, String type) async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      if (type == 'new_post') {
        sendFeatureEvent('new_post');
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (BuildContext context) => const CreatePostScreen(),
          ),
        );
      } else if (type == 'new_book') {
        sendFeatureEvent('new_book');
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return const CreateBookScreen();
            },
          ),
        );
      } else if (type == 'books') {
        sendFeatureEvent('books');
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return const BooksScreen();
            },
          ),
        );
      } else if (type == 'new_readingPlan') {
        sendFeatureEvent('new_readingPlan');
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return const CreateReadingPlanScreen();
            },
          ),
        );
      }
    } else {
      scaffoldKey.currentState?.closeDrawer();
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        duration: Duration(seconds: 3),
        content: Text(
            'There is no Internet connection.\nThe new post functionality is disabled'),
      ));
    }
  }
}
