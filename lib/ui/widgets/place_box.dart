import 'package:bookfriends/ui/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:bookfriends/core/models/place.dart';

const styleUsername = TextStyle(
  fontSize: 14,
  color: BookFriendsColor.socialPink,
);

class PlaceBox extends StatelessWidget {
  final Place place;

  const PlaceBox({required this.place, super.key});

  @override
  Widget build(BuildContext context) {
    return Transform.translate(
        offset: const Offset(0, 0),
        child: Card(
          elevation: 0,
          color: Colors.transparent,
          child: Align(
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 5, 5, 5),
                          child: Row(
                            children: [
                              SizedBox(
                                  width: 265,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        place.name,
                                        style: const TextStyle(
                                          fontSize: 16,
                                        ),
                                      ),
                                      Text(
                                        'Address: ${place.vicinity}',
                                        style: const TextStyle(
                                          fontSize: 14,
                                        ),
                                      )
                                    ],
                                  )),
                              SizedBox(
                                width: 60,
                                child: Text(
                                  '${place.distance}',
                                  style: styleUsername,
                                ),
                              ),
                              Text(
                                place.rating,
                                style: styleUsername,
                              )
                            ],
                          )))
                ],
              )),
        ));
  }
}
