import 'package:flutter/material.dart';
import 'package:bookfriends/core/models/place.dart';
import 'place_box.dart';

const styleListFriends = TextStyle(
    fontSize: 16,
    //color: Colors.white,
    fontWeight: FontWeight.bold);
const styleDescription = TextStyle(
  fontSize: 16,
);

class PlaceLayout extends StatelessWidget {
  final PlaceList places;
  final double distance;

  const PlaceLayout({required this.places, super.key, required this.distance});
  Widget _buildDivider() {
    return const Divider(
      thickness: 1.5,
      color: Colors.grey,
      height: 10.0,
    );
  }

  Widget _title() {
    return Row(
      children: const [
        SizedBox(
            width: 250,
            child: Center(
                child: Text(
              'Places',
              style: styleListFriends,
            ))),
        SizedBox(
          width: 60,
          child: Text(
            'Distance',
            style: styleListFriends,
          ),
        ),
        Expanded(
          child: Icon(
            Icons.star,
            size: 15,
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    String txt = '';
    bool thereAre = true;
    double newDistance = double.parse((distance).toStringAsFixed(2));

    List<Place> placesFilter = places.list
        .where(
          (element) => element.distance <= distance,
        )
        .toList();

    if (placesFilter.isEmpty) {
      txt = '\nThere are no bookstores in less than $newDistance Km from you\n';
      thereAre = false;
    } else if (placesFilter.length == 1) {
      txt = '\nThere is 1 bookstore in less than $newDistance Km from you:\n';
    } else {
      txt =
          '\nThere are ${placesFilter.length} bookstores in less than $newDistance Km from you:\n';
    }

    return Column(
      children: [
        Text(
          txt,
          style: styleDescription,
        ),
        Padding(
            padding: const EdgeInsets.fromLTRB(30, 5, 5, 5),
            child: thereAre ? _title() : const Text('')),
        ListView.separated(
          separatorBuilder: (context, index) => _buildDivider(),
          itemCount: placesFilter.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Column(
              children: [
                Center(
                    child: SizedBox(
                        width: 1000,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 8.0,
                          ),
                          child: PlaceBox(
                            place: placesFilter[index],
                          ),
                        )))
              ],
            );
          },
        ),
      ],
    );
  }
}
