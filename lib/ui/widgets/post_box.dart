import 'package:bookfriends/core/event_report.dart';
import 'package:bookfriends/core/models/post.dart';
import 'package:bookfriends/core/models/reading_plan.dart';
import 'package:bookfriends/ui/constants/colors.dart';
import 'package:bookfriends/ui/screens/reading_plan_screen.dart';
import 'package:bookfriends/ui/utils/parse_utils.dart';
import 'package:bookfriends/ui/widgets/profile_nav_wrapper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../core/models/user.dart';
import '../../core/providers/reading_plan_provider.dart';
import '../../core/providers/user_provider.dart';

class PostBox extends StatelessWidget {
  final Post post;

  const PostBox({required this.post, super.key});

  Widget _buildHeader() {
    return Selector<UserProvider, User>(
      selector: (_, userProvider) => userProvider.getUserById(post.userId),
      builder: (context, user, _) => Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          ProfileNavWrapper(
              user: user,
              child: CachedNetworkImage(
                imageUrl: '${user.imageUrl}',
                imageBuilder: (context, imageProvider) => Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
                fit: BoxFit.fill,
                placeholder: (context, url) => const CircleAvatar(
                  radius: 20,
                  backgroundImage:
                      AssetImage('assets/images/no_image_user.png'),
                ),
                errorWidget: (context, url, error) => const CircleAvatar(
                  radius: 20,
                  backgroundImage:
                      AssetImage('assets/images/no_image_user.png'),
                ),
              )),
          const SizedBox(
            width: 16.0,
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  user.name,
                  style: const TextStyle(
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.left,
                ),
                const SizedBox(
                  height: 2.0,
                ),
                Text(
                  ParseUtils.toDaysAgo(post.publicationDate),
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {},
            child: const Icon(
              Icons.more_vert_rounded,
              size: 16.0,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Text(
      post.content,
      style: const TextStyle(
        fontSize: 18.0,
      ),
    );
  }

  Widget _buildInteractionChild(IconData icon, String text) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          icon,
          size: 18.0,
        ),
        const SizedBox(
          width: 6.0,
        ),
        Text(
          text,
          style: const TextStyle(
            fontSize: 14.0,
          ),
        ),
      ],
    );
  }

  Widget _buildInteractions(BuildContext context) {
    return Row(
      children: [
        _buildInteractionChild(
          Icons.thumb_up_outlined,
          post.likes.toString(),
        ),
        const SizedBox(
          width: 12.0,
        ),
        _buildInteractionChild(
          Icons.comment_outlined,
          post.numComments.toString(),
        ),
        const SizedBox(
          width: 12.0,
        ),
        _buildInteractionChild(
          Icons.ios_share_outlined,
          post.numShares.toString(),
        ),
        const Spacer(),
        _readingPlanButton(),
      ],
    );
  }

  Widget _readingPlanButton() {
    return Selector<ReadingPlanProvider, ReadingPlan>(
      selector: (_, readingPlanProvider) =>
          readingPlanProvider.getReadingPlanById(post.readingPlanId),
      builder: (context, readingPlan, _) => ElevatedButton(
        onPressed: () {
          final readingPlanProvider2 = context.read<ReadingPlanProvider>();
          readingPlanProvider2.addView(readingPlan.id);
          sendReadingPlanClickEvent(readingPlan.title);
          Navigator.of(context).push(
            MaterialPageRoute<void>(
              builder: (BuildContext context) => ReadingPlanScreen(
                readingPlan: readingPlan,
              ),
            ),
          );
        },
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith(
              (_) => BookFriendsColor.socialPink),
        ),
        child: const Text(
          'Go to the plan',
          style: TextStyle(
            color: Colors.white,
            fontSize: 14.0,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _buildHeader(),
        const SizedBox(
          height: 16.0,
        ),
        _buildContent(),
        const SizedBox(
          height: 16.0,
        ),
        _buildInteractions(context),
      ],
    );
  }
}
