import 'package:bookfriends/ui/screens/profile_view.dart';
import 'package:flutter/material.dart';

import '../../core/event_report.dart';
import '../../core/models/user.dart';

class ProfileNavWrapper extends StatelessWidget {
  final Widget child;
  final User user;
  const ProfileNavWrapper({required this.child, required this.user, super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: child,
      onTap: () {
        sendFeatureEvent('see_profile');
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return ProfileView(
                user: user,
              );
            },
          ),
        );
      },
    );
  }
}
