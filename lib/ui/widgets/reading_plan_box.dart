import 'package:bookfriends/ui/widgets/reading_plan_nav_wrapper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../core/models/book.dart';
import '../../core/models/reading_plan.dart';
import '../../core/providers/book_provider.dart';

class ReadingPlanBox extends StatelessWidget {
  final ReadingPlan readingPlan;

  const ReadingPlanBox({required this.readingPlan, super.key});

  Widget _buildImage() {
    return Selector<BookProvider, Book>(
      selector: (_, bookProvider) =>
          bookProvider.getBookById(readingPlan.bookId),
      builder: (context, book, _) => ReadingPlanNavWrapper(
          readingPlan: readingPlan,
          child: CachedNetworkImage(
            imageUrl: book.imageUrl,
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
              ),
            ),
            fit: BoxFit.cover,
            placeholder: (context, url) =>
                Image.asset('assets/images/no_book.jpg'),
            errorWidget: (context, url, error) =>
                Image.asset('assets/images/no_book.jpg'),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: _buildImage()),
      ],
    );
  }
}
