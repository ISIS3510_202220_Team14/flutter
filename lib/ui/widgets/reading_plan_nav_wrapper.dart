import 'package:bookfriends/core/event_report.dart';
import 'package:bookfriends/core/models/reading_plan.dart';

import 'package:bookfriends/ui/screens/reading_plan_screen.dart';
import 'package:flutter/material.dart';

class ReadingPlanNavWrapper extends StatelessWidget {
  final Widget child;
  final ReadingPlan readingPlan;
  const ReadingPlanNavWrapper(
      {required this.child, required this.readingPlan, super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: child,
      onTap: () {
        sendReadingPlanClickEvent(readingPlan.title);
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (BuildContext context) => ReadingPlanScreen(
              readingPlan: readingPlan,
            ),
          ),
        );
      },
    );
  }
}
