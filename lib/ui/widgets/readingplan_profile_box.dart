import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:bookfriends/core/models/reading_plan.dart';
import 'package:provider/provider.dart';
import '../../core/event_report.dart';
import '../../core/models/book.dart';
import '../../core/providers/book_provider.dart';
import '../screens/reading_plan_screen.dart';

class ReadingPlanProfileBox extends StatelessWidget {
  final ReadingPlan readingPlan;

  const ReadingPlanProfileBox({required this.readingPlan, super.key});

  @override
  Widget build(BuildContext context) {
    return Selector<BookProvider, Book>(
        selector: (_, bookProvider) =>
            bookProvider.getBookById(readingPlan.bookId),
        builder: (context, book, _) => Transform.translate(
            offset: const Offset(0, 0),
            child: Card(
              elevation: 0,
              color: Colors.transparent,
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(25, 5, 5, 5),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                            icon: CachedNetworkImage(
                              imageUrl: book.imageUrl,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: 50.0,
                                height: 150.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.fitHeight),
                                ),
                              ),
                              fit: BoxFit.fitHeight,
                              placeholder: (context, url) =>
                                  Image.asset('assets/images/no_book.jpg'),
                              errorWidget: (context, url, error) =>
                                  Image.asset('assets/images/no_book.jpg'),
                            ),
                            iconSize: 50,
                            onPressed: () {
                              sendFeatureEvent('see_readingPlan');
                              Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                  builder: (BuildContext context) =>
                                      ReadingPlanScreen(
                                    readingPlan: readingPlan,
                                  ),
                                ),
                              );
                            },
                          ),
                          Expanded(
                              child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 5, 5, 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(book.name,
                                          style: const TextStyle(
                                            fontSize: 16,
                                          )),
                                      Text(
                                          'Started: ${readingPlan.startDate.day}-${readingPlan.startDate.month}-${readingPlan.startDate.year}',
                                          style: const TextStyle(
                                            fontSize: 14,
                                          ))
                                    ],
                                  )))
                        ],
                      ))),
            )));
  }
}
