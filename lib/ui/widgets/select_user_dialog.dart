import 'package:bookfriends/core/models/user.dart';
import 'package:bookfriends/core/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SelectUserDialog extends StatefulWidget {
  const SelectUserDialog({super.key});

  @override
  State<SelectUserDialog> createState() => _SelectUserDialogState();
}

class _SelectUserDialogState extends State<SelectUserDialog> {
  final TextEditingController _searchNameController = TextEditingController();

  // Consts
  static const _verticalSpace = SizedBox(height: 16.0);

  List<String>? _resultUsersIds;

  Widget _buildTextField({required TextEditingController controller, String? label}) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
        border: const OutlineInputBorder(),
        alignLabelWithHint: true,
        labelText: label,
      ),
      onChanged: (value) async {
        final userProvider = context.read<UserProvider>();

        if (value.isEmpty) {
          setState(() {
            _resultUsersIds = null;
          });
        } else {
          final searchUsersIds = await userProvider.searchUsersByName(value);
          setState(() {
            _resultUsersIds = searchUsersIds;
          });
        }
      },
    );
  }

  Widget _buildUsersListView() {
    return SizedBox(
      height: 160.0,
      child: Selector<UserProvider, List<User>>(
        selector: (_, userProvider) =>
            _resultUsersIds == null ? userProvider.users : userProvider.getUsersByIds(_resultUsersIds!),
        builder: (context, users, _) => ListView.builder(
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemCount: users.length,
          itemBuilder: (context, index) {
            final user = users.elementAt(index);
            return SizedBox(
              width: 120.0,
              child: InkWell(
                onTap: () => Navigator.of(context).pop(user),
                child: Card(
                  elevation: 5.0,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(children: [
                      Expanded(
                        child: CircleAvatar(
                          radius: double.infinity,
                          backgroundImage: Image.network(
                            '${user.imageUrl}',
                            isAntiAlias: true,
                          ).image,
                        ),
                      ),
                      _verticalSpace,
                      Text(
                        user.name,
                        textAlign: TextAlign.center,
                      ),
                    ]),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildContent() {
    return ListView(
      shrinkWrap: true,
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.symmetric(
        vertical: 24.0,
        horizontal: 32.0,
      ),
      children: [
        const Text(
          'Select an user',
          style: TextStyle(
            fontWeight: FontWeight.w600,
          ),
        ),
        _verticalSpace,
        _buildTextField(
          controller: _searchNameController,
          label: 'Search by name',
        ),
        _verticalSpace,
        _buildUsersListView(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildContent();
  }
}
